.POSIX:

PANDOC=pandoc
PANFLAGS=--mathml
AWK=awk

#
SRCS=blog/unixv6-alloc.md    \
     blog/shell-scripting.md \
     blog/nano-config.md     \
     blog/posix-c-types.md   \
     blog/nasm-stabs.md      \
     blog/yubikey.md         \
     blog/rust-async.md      \
     blog/latex-unicode-setminus.md \
     blog/ethical-ai.md \
     blog/realforce_r3.md \
     blog/2023-37c3-fp-asm.md \
     blog/fediverse.md \
     blog/concerned-citizens.md
_SRCS=2023-kas-talk-ai-ethics/index.md 2023-hoc-pl-speed-dating/index.md \
      index.md

#
all: $(SRCS:.md=.html) $(_SRCS:.md=.html) blog/index.html

# Main website landing page
# index.html

# Index generation
blog/index.html: $(SRCS:.md=.idx)
	$(PANDOC) $(PANFLAGS) -s -H header.html \
	                         -M title="Blog Posts" \
	                         -o $@ \
	                         -f markdown $^

# suffix rules
.md.idx:
	$(PANDOC) $(PANFLAGS) --template idx-entry.md --data-dir .\
	                      -V URI=$(*F).html \
	                      -o $@ \
	                      -t markdown $^
.md.html:
	$(PANDOC) $(PANFLAGS) -s -H header.html \
	                           -o $@ $^

# specials

blog/nasm-stabs.md: blog/nasm-stabs/index.html
blog/nasm-stabs/index.html: blog/nasm-stabs/index.md
	$(PANDOC) $(PANFLAGS) -s -H header.html \
	                         -M title="NASM+STABS Misc. Files" \
	                         -o $@ \
	                         -f markdown $^
blog/nasm-stabs/index.md:
	$(MAKE) -C blog/nasm-stabs index.md

#
clean:
	$(RM) *.html *.idx

#
.PHONY: all clean
.SUFFIXES:
.SUFFIXES: .md .html .idx
