---
title: "KAS Impuls: Should AI Meet Regulation -- eine ethische Perspektive"
authors:
 - Janis König
date: "19. Sep 2023"
header-includes: |
  <style>
  
  </style>
---

Auf dieser Seite habe ich einige Hintergrundinformationen und weiterführende Artikel
zu meinem Impuls-Talk im KAS verlinkt.
Ich beschränke mich hierbei vor Allem auf technologiekritische Links,
da es meiner Einschätzung nach "positive" und tlw. geradezu unkritische Artikel zu genüge gibt.

# Challenges

 * Wie viel Kontrolle müssen denn über ein Werkzeug haben,
   damit der Begriff "Werkzeug" nicht zur Farce wird?
 * Wer soll Verantwortung an der KI's statt übernehmen,
   wenn für reine Nutzener\*innen diese eine Black-Box ohne Möglichkeit zur Introspektive ist?
 * Wie soll so eine "Letztentscheidung" getroffen sind,
   wenn wir Menschen durch die KI-Vorentscheidung "[geprimed](https://de.wikipedia.org/wiki/Priming_(Psychologie))" werden?
 * Wie stellen wir sicher, dass sich unsere Kunst & Kultur weiterentwickelt,
   wenn KI so viel billiger ist?
 * Oder auch, dass sich nur die, die ausgesorgt haben, sich Leisten können sich künstlerisch zu betätigen?
 * Wie bauen wir eine Zukunft, in der wir nicht alle schönen Dinge automatisiert haben;
   aber die lästige Arbeit weiter manuell verrichten müssen?
 * Wie können wir sicherstellen,
   dass KI-Firmen durch ihre Marktdominanz nicht Künstler\*innen in die Abhängigkeit treiben.

# Linkliste: Ethik

 * [Studie: AI Art and its Impact on Artists](https://doi.org/10.1145/3600211.3604681)
 * [Hintergrundartikel: The Dark Past of Algorithms That Associate Appearance and Criminality](https://www.americanscientist.org/article/the-dark-past-of-algorithms-that-associate-appearance-and-criminality)
 * [Hintergrundartikel: If a hammer was like AI…](https://axbom.com/hammer-ai/)
 * [Hintergrundartikel: Language Is a Poor Heuristic For Intelligence](https://karawynn.substack.com/p/language-is-a-poor-heuristic-for)
 * [Buch (neu): Your Face Belongs to Us](https://www.penguinrandomhouse.com/books/691288/your-face-belongs-to-us-by-kashmir-hill/)
 * [Buch: The Intelligence Illusion](https://illusion.baldurbjarnason.com/)
 * [Buch: Hello World](https://www.chbeck.de/fry-hello-world/product/26909221)
 * [Eigener Blog-Artikel: Rules of Ethical AI](../ethical-ai.html) 


# Weiterführendes

 * Wie KI zu schlechteren Lösungsansätzen führen kann, oder "The right algorithm in the right place beats an inscrutable pile of ReLUs every damned time.":
   [Studie: “Low-Resource” Text Classification: A Parameter-Free Classification Method with Compressors](http://dx.doi.org/10.18653/v1/2023.findings-acl.426)
 * Die falschen Versprechungen von KI durch Technologiefirmen:
   [Hintergrundartikel: The Myth of Artificial Intelligence](https://prospect.org/culture/books/myth-of-artificial-intelligence-kissinger-schmidt-huttenlocher/)
 * Angst vor KI und Technologie hängt mit Abstiegsängsten zusammen:
   [Hintergrundartikel: Ted Chiang: Fears of Technology Are Fears of Capitalism](https://kottke.org/21/04/ted-chiang-fears-of-technology-are-fears-of-capitalism)
 * Der Effekt von KI generierten Inhalten auf Menschen ähnelt dem eines geschickten Tricksers
   [Hintergrundartikel: The LLMentalist Effect: how chat-based Large Language Models replicate the mechanisms of a psychic’s con](https://softwarecrisis.dev/letters/llmentalist/)
 * Die tatsächliche Produktivitätssteigerung durch KIs ist fragwürdig
   [The hard truth about productivity research](https://www.baldurbjarnason.com/2023/ai-research-again/)
 * Luddites, und warum technischer Fortschritt immer sozial begleitet werden muss:
   [I’ve always loved tech. Now, I’m a Luddite. You should be one, too.](https://www.washingtonpost.com/opinions/2023/09/18/luddites-social-technology-visionaries/)

# Zitate

 * _"The right algorithm in the right place beats an inscrutable pile of ReLUs every damned time."_ ~[Simon Cozens](https://typo.social/@simoncozens/110708666170433777)
 * _"[AI is] It's a DDoS attack on written knowledge."_ ~[Rich Felker](https://hachyderm.io/@dalias/110528502657419999)
 * _"I’ve never before experienced such a disparity between how much potential I see in a technology and how little I trust those who are making it."_ ~Baldur Bjarnason, o.g. Buch

# Rekursion: QR-Code hierher

![`https://www.blog.ljrk.org/2023-kas-talk-ai-ethics`](qr-code.png){ style="display: block; margin-left: auto; margin-right: auto;"}
