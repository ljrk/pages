---
title: "lj's Web Space"
---

Welcome to my little space on the web.
You can find my Blog on the `/blog` subdirectory or by clicking
[here](https://ljrk.codeberg.page/blog)

If you want to learn more about me, here's some kind of bio:

 * Names & Pronouns: lj/ElleJay (they/them, any), Janis (she) or Leo (he)
 * Interests: Typ~~o~~esetting (LaTeX, ConTeXt, Typst, roff, Markdown), Automation, Software Verification, Taint Analysis & Code Review, Immutable OS, FIDO & Passkeys, Reverse Engineering, Learning & Teaching, Cryptography, ...
 * Favorite editor: ~~vim, atom, neovim, kak, vi, vscode, vim, emacs~~ I gave up and use vi/vscode/emacs in rotation.
 * OS: fedora Silverblue and ArchLinux
 * Programming Languages: C (most skill), Rust (most awe), Haskell (most passion), Python (most hatred), POSIX Shell (most kink)

My vocation lies in teaching and I spend a considerable amount of my personal time helping people understand security better, introducing them to hacking, or teaching coding.
