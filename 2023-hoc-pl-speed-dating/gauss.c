#include <inttypes.h>
#include <stdint.h>

int64_t gauss(const int64_t n)
{
	return (n * (n+1)) / 2;
}
