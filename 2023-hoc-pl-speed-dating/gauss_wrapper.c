#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

extern int64_t gauss(const int64_t n);

int main(const int argc, const char *const argv[const argc+1])
{
	if (argc != 2) {
		fprintf(stderr, "Expecting precisely one argument\n"
		                "\n"
		                "Usage: %s <an integer>\n", argv[0]);
		exit(1);
	}

	/* yes, this doesn't do error handling pretty much at all :-) */
	const int64_t n = atoi(argv[1]);
	const int64_t sum = gauss(n);
	printf("sum(%"PRId64") = %"PRId64"\n", n, sum);
}
