---
title: "Heart of Code: Programming Language Speed Dating 2023"
authors:
 - Janis König
date: "19. Nov 2023"
header-includes: |
  <style>
  
  </style>
---

My inputs/pitches for Shell/AWK, C and Assembly at HoC's PL Speed Dating:
[Slides](slides.html)

# Shell/AWK

### Setup

Any macOS or Linux system (or Windows with WSL) should work without further setup :-)

### Tutorial

My recommended tutorial is the first chapter of "The AWK Programming Language".
The book can be found online, but I also bring a copy of the 2nd edition in paper.

# C

### Setup

Linus (or WSL) simply requires installing a C compiler such as GCC. In doubt, the following will probably work:
```
$ sudo apt install build-essential
```

On macOS, install the XCode Command Line Tools using
```
$ xcode-select --install
```
or, use brew/nix if installed to install GCC/Clang.

### Tutorial

Similarly, I recommend the first chapter of "The C Programming Language, 2nd Ed.", which can be found online.

# Assembly

### Setup

We'll do Intel x86 Assembly, which in most cases requires identical setup as C (see above),
additionally install your favorite assembler, e.g., NASM:
```
$ sudo apt install nasm
```

In case you have a different CPU architecture (mobile device or "Apple Silicon" macBook),
you can use a VM (or container) with x86 Linux.

### Tutorial

The goal is to implement a function called `gauss` in file called `gauss.asm` that takes a 64-bit integer $n$
and returns the sum of all integers $i\leq n$, using the formula for the triangular numbers (also
known as Small Gauss' Sum):
$$
	\sum_i^n i = \frac{n(n+1)}{2}
$$

[This introduction](https://codeberg.org/ljrk/ca-oscn-supps/releases/download/v1.2/supp-gauss.pdf) will introduce you
to the fundamentals of x86 assembly and guide you along.
You'll also need an additional C file which will process the user input,
calculate the sum using your function, and finally print the result:

 * [gauss_wrapper.c](gauss_wrapper.c)

# Recursion

![`https://www.blog.ljrk.org/2023-hoc-pl-speed-dating`](qr-code.png){ style="display: block; margin-left: auto; margin-right: auto;"}
