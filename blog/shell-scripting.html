<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="ljrk" />
  <meta name="dcterms.date" content="2021-05-13" />
  <meta name="keywords" content="unix, sh, programming, sed, grep, awk, scripting" />
  <title>Intro to Shell Scripting</title>
  <style>
    html {
      color: #1a1a1a;
      background-color: #fdfdfd;
    }
    body {
      margin: 0 auto;
      max-width: 36em;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 50px;
      padding-bottom: 50px;
      hyphens: auto;
      overflow-wrap: break-word;
      text-rendering: optimizeLegibility;
      font-kerning: normal;
    }
    @media (max-width: 600px) {
      body {
        font-size: 0.9em;
        padding: 12px;
      }
      h1 {
        font-size: 1.8em;
      }
    }
    @media print {
      html {
        background-color: white;
      }
      body {
        background-color: transparent;
        color: black;
        font-size: 12pt;
      }
      p, h2, h3 {
        orphans: 3;
        widows: 3;
      }
      h2, h3, h4 {
        page-break-after: avoid;
      }
    }
    p {
      margin: 1em 0;
    }
    a {
      color: #1a1a1a;
    }
    a:visited {
      color: #1a1a1a;
    }
    img {
      max-width: 100%;
    }
    svg {
      height: auto;
      max-width: 100%;
    }
    h1, h2, h3, h4, h5, h6 {
      margin-top: 1.4em;
    }
    h5, h6 {
      font-size: 1em;
      font-style: italic;
    }
    h6 {
      font-weight: normal;
    }
    ol, ul {
      padding-left: 1.7em;
      margin-top: 1em;
    }
    li > ol, li > ul {
      margin-top: 0;
    }
    blockquote {
      margin: 1em 0 1em 1.7em;
      padding-left: 1em;
      border-left: 2px solid #e6e6e6;
      color: #606060;
    }
    code {
      font-family: Menlo, Monaco, Consolas, 'Lucida Console', monospace;
      font-size: 85%;
      margin: 0;
      hyphens: manual;
    }
    pre {
      margin: 1em 0;
      overflow: auto;
    }
    pre code {
      padding: 0;
      overflow: visible;
      overflow-wrap: normal;
    }
    .sourceCode {
     background-color: transparent;
     overflow: visible;
    }
    hr {
      background-color: #1a1a1a;
      border: none;
      height: 1px;
      margin: 1em 0;
    }
    table {
      margin: 1em 0;
      border-collapse: collapse;
      width: 100%;
      overflow-x: auto;
      display: block;
      font-variant-numeric: lining-nums tabular-nums;
    }
    table caption {
      margin-bottom: 0.75em;
    }
    tbody {
      margin-top: 0.5em;
      border-top: 1px solid #1a1a1a;
      border-bottom: 1px solid #1a1a1a;
    }
    th {
      border-top: 1px solid #1a1a1a;
      padding: 0.25em 0.5em 0.25em 0.5em;
    }
    td {
      padding: 0.125em 0.5em 0.25em 0.5em;
    }
    header {
      margin-bottom: 4em;
      text-align: center;
    }
    #TOC li {
      list-style: none;
    }
    #TOC ul {
      padding-left: 1.3em;
    }
    #TOC > ul {
      padding-left: 0;
    }
    #TOC a:not(:hover) {
      text-decoration: none;
    }
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    div.columns{display: flex; gap: min(4vw, 1.5em);}
    div.column{flex: auto; overflow-x: auto;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    /* The extra [class] is a hack that increases specificity enough to
       override a similar rule in reveal.js */
    ul.task-list[class]{list-style: none;}
    ul.task-list li input[type="checkbox"] {
      font-size: inherit;
      width: 0.8em;
      margin: 0 0.8em 0.2em -1.6em;
      vertical-align: middle;
    }
  </style>
  <style type="text/css">
      /* http://web.simmons.edu/~grabiner/comm244/weekfour/code-test.html */
      pre code {
          padding-left: 40px;
          display: block;
          font-size: 70%;
          white-space: pre;
      }
  </style>
  <!-- ugly hack for link verification :3 -->
  <a rel="me" href="https://todon.eu/@ljrk"></a>
</head>
<body>
<header id="title-block-header">
<h1 class="title">Intro to Shell Scripting</h1>
<p class="author">ljrk</p>
<p class="date">2021-05-13</p>
</header>
<p>I’m regularly asked to write something about the magic of shell
scripting, so here goes. While I don’t expect deep understanding from
the reader, I assume basic knowledge of how to work with the terminal
itself and having seen some scripts (while maybe being too scared to
touch them yet).</p>
<p>Shell scripting is different from other scripting or programming in
that we don’t have “libraries” we include. Instead, all programs we have
installed serve as our huge library of tools we can invoke, chain
together, loop over, etc. Thus, “learning shell scripting” consists of
a) learning the tools commonly available on your regular UNIX/Linux
workstation, and b) learning the language that chains together these
tools.</p>
<p>For the language we will use the POSIX Shell subset <a href="#fn1"
class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>
that’s virtually supported by any shell, including Bash, Zsh, but also
more modern incarnations of Ksh. This isn’t only a plus due to
portability, but also because POSIX Shell is <em>much</em> more simple
than the many different ways we can build <code>if</code> in Bash, or
iterate in Zsh. While they are definitely useful in some contexts, most
often the multitude of syntaxes only confuse the user.</p>
<h1 id="posix-shell">POSIX Shell</h1>
<h2 id="if-test">If &amp; Test</h2>
<p>Probably the most esoteric part of the classic shell is
<code>if</code> in combination with the <code>test</code> program. Ksh,
Bash, Zsh and so on all set out to “fix” this, however, the added
complexity made things, arguably, worse. And while definitely an
idiosyncratic design, it’s rather easy to understand, so let’s
start:</p>
<p>The <code>if</code> built-in keyword simply executes a
<em>program</em> and checks its exit code. If the program exited with
code 0, this is considered to be a true condition. Or, as described more
verbosely in the standard under <strong>The if Conditional
Construct</strong>:</p>
<blockquote>
<p>The if compound-list shall be executed; if its exit status is zero,
the then compound-list shall be executed and the command shall complete.
Otherwise, each elif compound-list shall be executed, in turn, and if
its exit status is zero, the then compound-list shall be executed and
the command shall complete. Otherwise, the else compound-list shall be
executed.</p>
</blockquote>
<p>In most environments you will have two programs called
<code>true</code> and <code>false</code> available at
<code>/bin/true</code> &amp; <code>/bin/false</code> or
<code>/usr/bin/true</code> and <code>/usr/bin/false</code> respectively.
Let’s check what exit-code they have! You can either enter
<code>sh</code> to get a POSIX interactive shell and type the following
directly, or save it as a file, e.g., <code>foo.sh</code> and run it as
<code>sh ./foo.sh</code>:</p>
<pre><code>if /usr/bin/true; then
    echo &#39;exit code 0!&#39;
else
    echo &#39;exit code non-zero!&#39;
fi</code></pre>
<p>It prints “exit code 0!” which makes sense since the executable is
called “true”.</p>
<p>More commonly, however, we don’t want to check the exit code of a
program, but check the value of a variable. We can reduce this problem
to the checking of the exit code, if we’d have a program which takes an
expression and exits with the appropriate exit-code. Luckily for us,
someone already went through the hassle of writing this and called the
program <code>test</code>. Let’s give it a ride:</p>
<pre><code>answer=42
if test &quot;$answer&quot; -eq 42; then
    echo &quot;The Answer is $answer&quot;
fi</code></pre>
<p>While working, this looks a bit clumsy, so the shorthands
<code>[</code> and <code>]</code> were created as alternative names and
delimiters to <code>test</code>:</p>
<pre><code>answer=42
if [ &quot;$answer&quot; -eq 42 ]; then
    echo &quot;The Answer is $answer&quot;
fi</code></pre>
<p>Since <code>[</code> is a program with the arguments
<code>"$answer"</code> (shell-expanded to the value of the variable),
<code>-eq</code>, <code>42</code> and <code>]</code> you need to
separate all of these with spaces. The following <em>does not
work!</em>:</p>
<pre><code>answer=42
if [&quot;$answer&quot; -eq 42]; then
    echo &quot;The Answer is $answer&quot;
fi</code></pre>
<h3 id="multiple-conditions">Multiple Conditions</h3>
<p>In order to check for the truth value of multiple conditions, we call
<code>test</code> multiple times, chaining the results:</p>
<pre><code>if test &quot;$answer&quot; -eq 42 &amp;&amp; test &quot;$earth&quot; = &quot;exploded&quot;; then
    echo &quot;BOOOM&quot;
fi</code></pre>
<p>or, with the prettier <code>[]</code>-Syntax:</p>
<pre><code>if [ &quot;$answer&quot; -eq 42 ] &amp;&amp; [ &quot;$earth&quot; = &quot;exploded&quot; ]; then
    echo &quot;BOOOM&quot;
fi</code></pre>
<p>Depending on your previous knowledge, the <code>&amp;&amp;</code> may
already be known to you. While it acts as a logical-and here, it’s
semantics are slightly different: The command on the left-hand side of
the <code>&amp;&amp;</code> is executed, if it exited successfully
(i.e., exit status is zero), the right-hand side is executed as well
with the exit-status of the complete expression being the the
latter.</p>
<p>But, if the first command did fail, the second command will not be
executed, and failure signaled with a non-zero exit status.</p>
<p>Analogously, we can produce or using <code>||</code> which
short-curcuits as well, i.e., stops after the first command exited
successfully.</p>
<p>For completeness sake, there are also <strong>Sequential
Lists</strong> using <code>;</code> which simply executed the commands
in order, without exiting early and simply passing the last command in
the list. More advanced are <strong>Asynchronous Lists</strong> (using a
single <code>&amp;</code>) which run commands in the background,
(possibly) in parallel but are not appropriate for usage in
<code>if</code>, since they always exit with 0.</p>
<h3 id="else-else-if-and-empty-bodies">Else, Else-If and Empty
Bodies</h3>
<p>We can also do else blocks as well as else-if blocks, however, typing
is hard, and Shell syntax even worse, which is why:</p>
<pre><code>if [ &quot;$answer&quot; -eq 42 ];
    echo &quot;Answer given in decimal&quot;
else if [ &quot;$answer&quot; -eq 101010 ];
    echo &quot;Answer given in binary&quot;
fi</code></pre>
<p>doesn’t work—we must type less and use the keyword <code>elif</code>
instead:</p>
<pre><code>if [ &quot;$answer&quot; -eq 42 ];
    echo &quot;Answer given in decimal&quot;
elif [ &quot;$answer&quot; -eq 101010 ];
    echo &quot;Answer given in binary&quot;
else
    :
fi</code></pre>
<p>Further, if we have an empty body, we cannot just leave it empty, the
shell expects something. Luckily, the <code>:</code> serves as a
no-op.</p>
<h2 id="while--until-loops">While- &amp; Until-Loops</h2>
<p>The <code>while</code> loop works almost identical to the
<code>if</code> construct with the slight adjustment that the command
specified (i.e., most commonly <code>test</code>) is called multiple
times:</p>
<pre><code>while [ &quot;$answer&quot; -ne 42 ]; do
    echo &quot;Wrong answer... increasing&quot;
    answer=&quot;$((answer + 1))&quot;
done</code></pre>
<p>This uses <strong>Arithmetic Expansion</strong> using the
<code>$((...))</code> syntax. Within these we do <em>not</em> need
<code>$</code> to refer to variables and can now do pretty complex
maths, directly from the console, neat!</p>
<p>Similar to the <code>while</code> loop, we can use the
<code>until</code> loop:</p>
<pre><code>until [ &quot;$answer&quot; -eq 42 ]; do
    echo &quot;Wrong answer... increasing&quot;
    answer=&quot;$((answer + 1))&quot;
done</code></pre>
<h2 id="for-loops">For-Loops</h2>
<p>The <code>for</code> loop is probably the most avant-garde construct
of the shell as it is a range-based for-in loop, unlike a
three-expression-style as in C:</p>
<pre><code>for x in foo bar baz; do
    echo &quot;$x&quot;
done</code></pre>
<p>The syntax is quite easy to pick up and using the program
<code>seq</code> we can also iterate over indices:</p>
<pre><code>for i in $(seq 1 42); do
    echo &quot;$i&quot;
done</code></pre>
<p>Since the <code>for</code> loop doesn’t expect to run a program as
part of its “head” (unlike <code>if</code>) we need to explicitly ask
the shell to do <strong>Command Substitution</strong> using the
<code>$(...)</code> construct which runs the program with the specified
arguments and replaces the expression with the <em>output</em> of it
(not the exit-code, again, unlike <code>if</code>). Since
<code>seq</code> produces a list of numbers from 1 to 42 inclusive when
called like above, <code>i</code> will take precisely these values.</p>
<p>However, unlike our first example, the numbers produced aren’t
delimited by spaces but by newlines! Indeed, tabs would’ve worked just
as well. The shell does something called <strong>Field
Splitting</strong> here, and, by default, fields are split at space, tab
or newline. Again, quoting the standard:</p>
<blockquote>
<p>any sequence of <space>, <tab>, or <newline> characters at the
beginning or end of the input shall be ignored and any sequence of those
characters within the input shall delimit a field.</p>
</blockquote>
<p>We can actually modify at what position fields are split, e.g., for
parsing semicolon-delimited CSVs using <code>IFS=';'</code>, but this is
out of scope for this article :-)</p>
<h2 id="a-curious-case">A Curious Case</h2>
<p>In some cases you want to check the value of one variable against a
whole range of patterns. In many languages this can be done using a
switch-case or match construction. Since shell is a language for those
who don’t like to type much, only say <code>case</code> and terminate
the construct with <code>esac</code> (“case” reversed).</p>
<pre><code>case &quot;$x&quot; in
    foo) &quot;x is foo&quot; ;;
    bar|baz) &quot;x is bar or baz&quot; ;;
    *) &quot;x ain&#39;t no hoopy frood&quot; ;;
esac</code></pre>
<p>We can match everything, using the glob character <code>*</code>.</p>
<h2 id="quoting">Quoting</h2>
<p>You can see that I quoted the variable <code>x</code> in the body of
the example for loop:</p>
<pre><code>for x in foo bar baz; do
    echo &quot;$x&quot;
done</code></pre>
<p>In this case there’d have been no difference if I’d have ommitted the
quotes, but it is often considered good style to use them everywhere
where you can.</p>
<p>To demonstrate the difference, let’s replace the first value (foo) of
the list we iterate over by the string “The world is ending” which
contains spaces. In order to tell the loop that we consider this one
item of the list (and not four), we put quotes around it:</p>
<pre><code>for x in &quot;The world is ending&quot; bar baz; do
    printf &quot;Found: %s\n&quot; $x
done</code></pre>
<p>I also replaced the <code>echo</code> with a <code>printf</code> to
highlight the issue we will now observe: The output is:</p>
<pre><code>Found: The
Found: world
Found: is
Found: ending
Found: bar
Found: baz</code></pre>
<p>But… didn’t we ask the <code>for</code> to consider this as just one
item? We did, but I also sneakily removed the quotes around
<code>$x</code>, leading to the following chain of executed
commands:</p>
<pre><code>printf &quot;Found: %s\n&quot; The world is ending
printf &quot;Found: %s\n&quot; bar
printf &quot;Found: %s\n&quot; baz</code></pre>
<p>That is, the <code>printf</code> is executed with five arguments, the
format string (first) plus the four additional strings. However, we only
used one format specifier <code>%s</code> and thus expected just one
string following the format. This is the culprit here, as
<code>printf</code> has a rather unexpected behavior if passed more
arguments than allowed for in the format string.</p>
<p>The correct command execution would’ve been with quotes:</p>
<pre><code>printf &quot;Found: %s\n&quot; &quot;The world is ending&quot;
printf &quot;Found: %s\n&quot; &quot;bar&quot;
printf &quot;Found: %s\n&quot; &quot;baz&quot;</code></pre>
<p>Which can be achieved by quoting the <code>$x</code>.</p>
<p>Indeed, I recommend quoting <em>all</em> variables by default, and
only thinking of it as “when <em>must</em> I omit the quotes” instead of
the other way around.</p>
<p>However, there are also single-quotes which we didn’t talk about yet.
All strings within double-quotes are subject to <strong>Word
Expansions</strong>, that is, we could write:</p>
<pre><code>echo &quot;$answer&quot;</code></pre>
<p>To print the value of the variable <code>answer</code> since the
shell expanded it <em>before</em> passing the resulting string to
<code>echo</code>. Sometimes, we don’t want things like these to happen,
and actually want to print, say, a dollar sign:</p>
<pre><code>echo &#39;Your life is worth $0.02&#39;</code></pre>
<p>If we’d have used double quotes here, our shell would’ve been very
confused. In fact, many cases, like the <code>printf</code> format
strings above, we could (and possibly should) use single quotes to
prevent errornous expansion(s).</p>
<p>We can also nest quotes, use escape sequences, etc., but this is
again out-of-scope for this article.</p>
<h1 id="tools">Tools</h1>
<p>We’ve now had a brief look at some of the most simple constructs of
the POSIX Shell, but it is, by itself, not that powerful. We need the
tools of the UNIX workbench in order to do any useful composition using
the shell language.</p>
<h2 id="echoprintf">ECHO/PRINTF</h2>
<p>While using <code>echo</code> is simple, unfortunately, for all more
advanced usages, the exact behavior of <code>echo</code> is different
from platform to platform. Thus, if you do something differently than
echoing a simple variable or printing simple text, use
<code>printf</code> instead.</p>
<h2 id="grep">GREP</h2>
<p>The tool <code>grep</code> had it’s origins in the line editor
<code>ed</code>, from the editor command <code>g/re/p</code>, meaning,
“work globally”, “match by regular expression given as <code>re</code>”,
and “print the resulting lines”.</p>
<p>Spinned out as its own command-line tool, we can do just that,
without learning The Standard Editor (which is the precursor to ex,
precursor to vi, precursor to vim, precursor(?) to nvim). Most usage of
grep boils down to learning regular expressions, which is out of scope
of this article. However, I want to give some notes that many seem not
to be aware of:</p>
<ul>
<li>grep can work on multiple patternd <em>expressions</em>, using e.g.,
<code>grep -e pattern1 -e pattern2</code></li>
<li>grep has two different regex languages, you can use <code>-E</code>
to switch to extended regular expressions which require less
quoting.</li>
<li>you can invert the match using <code>-v</code>.</li>
</ul>
<p>Whatever you do with grep, remember though that it works on lines,
due to its heritage to <code>ed</code>.</p>
<h2 id="sed">SED</h2>
<p>The stream-editor <code>sed</code> also shares a heritage with
<code>ed</code>, basically being a simple scriptable version of it.
Instead of searching for a pattern and printing the results, we can
replace occurances, delete them, list them, print them, etc.</p>
<p>The most common usage is probably replacement, using the syntax of
<code>s/regexp/replacement/</code> with an optional trailing
<code>g</code> to replace <em>all</em> matches globally.</p>
<h2 id="tr">TR</h2>
<p>A specialisation to sed/grep is <code>tr</code>. Instead of replacing
one occurance with another string, we can replace ranges with other
ranges. E.g., to capitalize all the letters in a given text:</p>
<pre><code>echo &#39;The slow red tiger jumps over the energetic cat.&#39; | tr &#39;a-z&#39; &#39;A-Z&#39;</code></pre>
<p>Yielding</p>
<pre><code>THE SLOW RED TIGER JUMPS OVER THE ENERGETIC CAT</code></pre>
<h2 id="awk">AWK</h2>
<p>AWK supercharges the featureset of grep and sed by allowing us to
execute arbitrary code if a certain pattern is matched. That is, the
input is iterated over line by line, split into columns and you can
formulate patterns as well as conditions by referring to single columns
or the whole line. This is best understood in action, and, since I
cannot describe this any better, I copy this verbatim from the excellent
book “The AWK Programming Language” <a href="#fn2" class="footnote-ref"
id="fnref2" role="doc-noteref"><sup>2</sup></a> by Alfred V. Aho (The
Dragon Book on compiler design), Peter J. Weinberger, and Brian W.
Kernighan (“The C Programming Language”):</p>
<hr />
<p>Useful awk programs are often short, just a line or two. Suppose you
have a file called <code>emp.data</code> that contains the name, pay
rate in dollars per hour, and number of hours worked for your employees,
one employee per line, like this:</p>
<pre><code>Beth    4.00    0
Dan     3.75    0
Kathy   4.00    10
Mark    5.00    20
Mary    5.50    22
Susie   4.25    18</code></pre>
<p>Now you want to print the name and pay (rate times hours) for
everyone who worked more than zero hours. This is the kind of job that
awk is mneant for, so it’s easy. Just type this command line:</p>
<pre><code>awk &#39;$3 &gt; 0 { print $1, $2 * $3 }&#39; emp.data</code></pre>
<p>You should get this output:</p>
<pre><code>Kathy 40
Mark 100
Mary 121
Susie 76.5</code></pre>
<hr />
<p>Let’s analyze the program, given in the single quotes: The
<code>$3</code> refers to the third column, and thus the pattern matches
every line where the employee <code>$1</code> worked more than 0 hours.
In these cases, we execute the action given in the <code>{...}</code>,
printing the name, as well as the pay.</p>
<h2 id="find">FIND</h2>
<p>If awk matches patterns against lines in a file, <code>find</code>
matches patterns against files in your file system. As with awk, it can
execute code, when a pattern is matched, for example printing the line
count of every file with the extension <code>.c</code> in the current
directory, or any subdirectory:</p>
<pre><code>find . -name &#39;*.c&#39; -exec wc -l {} \;</code></pre>
<p>The expression <code>-name '*.c'</code> matches, and the expression
<code>-exec wc -l {} ;</code> executes the program <code>wc</code> with
the option <code>-l</code> (printing lines only), while substituting the
<code>{}</code> with eached matched file. Note that we need to escape
the <code>;</code> since <code>;</code> is a keyword in the shell
language (<code>';'</code> would’ve worked as well, but is one more
character to type). This results in e.g., the following executions:</p>
<pre><code>wc -l foo.c
wc -l src/bar.c</code></pre>
<p>With the output being:</p>
<pre><code>1312
161</code></pre>
<p>A bit unfortunate for us, however, the <code>wc</code> program now
prints only the line counts themselves, but we have a hard time
associating them with each file.</p>
<p>Most command-line tools are built “intelligently” though – they
change behavior, depending on whether they are called with multiple
arguments or just one. If we’d run:</p>
<pre><code>wc -l foo.c src/bar.c</code></pre>
<p>We’d get:</p>
<pre><code>foo.c:  1312
src/bar.c:  161</code></pre>
<p>How to achieve that with <code>find</code>? Well, asking
<code>find</code> nicely, would be a plus, so we replace the
<code>;</code> with a <code>+</code>, and behold:</p>
<pre><code>find . -name &#39;*.c&#39; -exec wc -l {} +</code></pre>
<p>Since the <code>+</code> is no shell keyword, we don’t need to escape
it either, neat!</p>
<p>With this, we can build powerful meta-tools, many of my personal
scripts are just wrappers around one powerful <code>find</code>
construct. And we don’t need to sin an use the non-POSIX GNU/grep
specific <code>grep -R</code> option, we can simply use the short:</p>
<pre><code>find . -type f -exec grep {} +</code></pre>
<p>Easy!</p>
<section id="footnotes" class="footnotes footnotes-end-of-document"
role="doc-endnotes">
<hr />
<ol>
<li
id="fn1"><p>https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html<a
href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn2"><p>https://9p.io/cm/cs/awkbook/index.html<a href="#fnref2"
class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
</body>
</html>
