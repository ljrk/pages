---
title: Setting Up A Fedora CoreOS Server
date: 2021-09-05
author:
- example.com[fedora, linux, container, podman, coreos, server]
---

I like the concept of Fedora CoreOS immutable systems where, instead of
traditional process-level separation we have something like "application"
or "container-level" separation.
And, while containers don't add that much in terms of security, they do add
quite much when it comes to being able to shift around applications and make
sure they don't interfere with each other (assuming they aren't malicious).

# Download & Installation

CoreOS is designed to be easily-deployable.
By that they don't mean an interactive installer but wide-scale deployments.
Ideally, a good deal of the configuration (including setting up ssh keys) is
done upfront via so-called ignition files which is just a fancy name for JSON.

Since JSON is quite annoying to type, the usual workflow is to write Butane
files and convert them to Ignition files.
Butane is just a fancy name for YAML.

A very simple Butane file configuring a computer called Earth with a user
called `core` and an ssh-key could look like this:

    variant: fcos
    version: 1.3.0
    passwd:
      users:
        - name: core
          ssh_authorized_keys:
            - ssh-ed25519 AAAA...
    storage:
      files:
        - path: /etc/hostname
          mode: 0644
          contents:
            inline: earth

We can convert this `.bu` file using the `butane` command-line tool which you
can either install via your preferred package manager or run using a Podman
container:

    $ podman run --interactive --rm --security-opt label=disable \
               --volume $(PWD):/pwd \
               --workdir /pwd \
               quay.io/coreos/butane:release --pretty --strict \
                   earth.bu > earth.ign

Now we have four options:

1. Hosting this Ignition file somewhere online and interactively pointing the
   server to it in the installation process,
2. Using an existing system to bootstrap onto a clean new disk,
3. Using our virtualisation tool to pass an additional startup-parameter
   declaring the Ignition file to use, or
4. Embedding the Ignition into the ISO.

The first option would require interactive sessions which is boring.
However, neither do I have an existing system (well I do, but that ain't fancy)
and my provider doesn't give me access to the QEMU startup arguments, so
option four it is.

## Embedding Installation Configuration Files

We now need the `coreos-installer` tool which can, as well, be installed
directly or run through a container.
First, we will download the "unconfigured" ISO:

    $ podman run --interactive --rm --security-opt label=disable \
                 --volume $(PWD):/pwd \
                 --workdir /pwd \
                 quay.io/coreos/coreos-installer:release download \
                     -s stable \
                     --architecture x86_64 \
                     -p metal \
                     -f iso

In my case, this downloaded a file called
"fedora-coreos-34.20210808.3.0-live.x86_64.iso".

The coreos-installer provides an easy way to embed an ignition file.
This ignition file, however, is not the one used for the installation process
but for the live system only!
So, if we'd use the above Ignition file, we'd set up a new "core" user for
the live installer ... how great.

What we instead want to do is create a new Butane configuration for the
live installer which

1. Actually kicks off the installation process
2. Embeds our "actual" server configuration Ignition file.

For my `earth_installer.ign` I will use the following:

    variant: fcos
    version: 1.3.0
    systemd:
      units:
        - name: install.service
          enabled: true
          contents: |
            [Unit]
            Description=Run CoreOS Installer
            Requires=coreos-installer-pre.target
            After=coreos-installer-pre.target
            OnFailure=emergency.target
            OnFailureJobMode=replace-irreversibly

            [Service]
            Type=oneshot
            ExecStart=/usr/bin/coreos-installer install -i /var/earth.ign /dev/sda
            ExecStart=/usr/bin/systemctl --no-block reboot
            StandardOutput=kmsg+console
            StandardError=kmsg+console

            [Install]
            RequiredBy=default.target
    storage:
      files:
        - path: /var/earth.ign
          mode: 0644
          contents:
            compression: null
            local: earth.ign

This first creates an automatically started install.service which will
use the ignition file placed at `/var/earth.ign` to bootstrap the system.
Secondly, it includes a file called `/var/earth.ign` from the local `earth.ign`
file.

    $ podman run --interactive --rm --security-opt label=disable \
                 --volume $(PWD):/pwd \
                 --workdir /pwd \
                 quay.io/coreos/butane:release --pretty --strict --files-dir .
                     earth_installer.bu > earth_installer.ign

This call to butane will actually copy the contents of the `earth.ign` file
base64 encoded into the resulting Ignition file.
For that to work we also need to add the `--files-dir .` option.

We can finally use this ignition file plus the downloaded ISO to produce our
custom installer

    $ podman run --interactive --rm --security-opt label=disable \
                 --volume $(PWD):/pwd \
                 --workdir /pwd \
                 quay.io/coreos/coreos-installer:release iso ignition embed
                     -o earth.iso fedora-coreos-34.20210808.3.0-live.x86_64.iso
                     < earth_installer.ign

Now we can simply boot our system with this ISO as startup media and it will
automatically run the setup process.

Remember to disable booting from DVD on the next boot, otherwise this will
loop and reinstall all the time!

# First access

We can now simply SSH onto the server with the private key corresponding to
the configured pubkey:

    $ ssh core@example.com
    Fedora CoreOS 34.20210808.3.0
    Tracker: https://github.com/coreos/fedora-coreos-tracker
    Discuss: https://discussion.fedoraproject.org/c/server/coreos/

    [core@earth ~]$ hostnamectl
     Static hostname: earth
           Icon name: computer-vm
             Chassis: vm
          Machine ID: ...
             Boot ID: ...
      Virtualization: kvm
    Operating System: Fedora CoreOS 34.20210808.3.0   
         CPE OS Name: cpe:/o:fedoraproject:fedora:34
              Kernel: Linux 5.13.7-200.fc34.x86_64
        Architecture: x86-64
     Hardware Vendor: ...
      Hardware Model: KVM Server


# User & Application Separation

On traditional UNIX setups for servers, different services were seperated
on a privilege-level using different users.
However, on a file-system level they all shared the host files, thus not
being as complete of an isolation as could be (if at least the host files could
be set read-only...).
This could lead to inconsistencies and difficulties upgrading single services
since they all might depend on some configuration file.

Containers were/are supposed to solve that, however, they usually run rootfull.
That is, the container itself is run as root which means that taking over
the container would be a de-facto root exploit [^1].
Linux has now, for some time, the option to create user namespaces, that is
unprivileged users can create new namespaces (i.e., containers) to run
isolated applications in.
So we now can create a new user, per application, which then each spawns one
or more containers.
A container engine vulnerability would only lead to the same privileges as
the user we've created.

Sadly, user namespaces in Linux were tacked on late and most "sensitive" code
in the Kernel assumes that it is called from a privileged user and thus not
necessarily written with security in mind.
If we now open up unprivileged users to call such code, the surface for
attacks grows.

It's a tough call to make:
Either we are trusting the Linux kernel to have been improved since the initial
inclusion of user-namespaces to also be hardened against unprivileged calls,
or we hope that our container engine is secure.

I chose the former, since I just feel better with the traditional UNIX approach
of one-user-per-service and the container layer is simply another layer I
tacked on.

# Services

The services (and thus users) that I'm going to host are:

* Webserver (Blog)
* Nextcloud
* OnlyOffice
* Paperless-NG

With Traefik as reverse-proxy to provide SSL/TLS.

So let's set them up:

    $ for u in traefik nc oo paperless; do
        sudo useradd -b /var/srv/ -m "$u";
        sudo install -d -o $u /run/user/$(id -u $u)
    done

The second command in the look is needed to create a runtime directory for the
new users.

## Traefik

<!--
While I said earlier that I'm going the rootless variant, the unprivileged
slirp4netns network stack isn't that well-polished yet.
This means that my reverse-proxy, while started/controlled by its own user,
will run rootfull.
The nice side-effect being that I can showcase both, rootfull and rootless
container setup.
-->

Since we are running Traefik as unprivileged user, we will need to allow
those to bind to ports lower than 1024.
We do that interactively by running

    $ sudo sysctl -w net.ipv4.ip_unprivileged_port_start=80

and make it permanent using:

    $ sudo echo 'net.ipv4.ip_unprivileged_port_start=80' | \
    $ sudo tee /etc/sysctl.d/99-unprivileged-port.conf

Before we start writing a configuration file for Traefik and automating
the startup, let's switch to the newly created user and run it manually:

    (core) $ sudo -iu traefik
    (traefik) $ systemctl --user start podman
    (traefik) $ podman run --rm \
               --security-opt label=disable \
               -v /var/run/user/1001/podman/podman.sock:/var/run/docker.sock:Z \
               -p 80:80 \
               -p 8080:8080 \
               traefik:v2.5 --api.insecure=true --providers.docker

Oh, `systemd` complains about:

    Failed to connect to bus: $DBUS_SESSION_BUS_ADDRESS and $XDG_RUNTIME_DIR not defined (consider using --machine=<user>@.host --user to connect to bus of other user)

The hint systemd gives isn't wrong, but a bit misleading in our case.
Systemd cannot connect to the user session, which is partly because `sudo -iu`
doesn't set the required environment variables to the user dbus UNIX socket.
This is, because `sudo` can be thought of "switching UIDs" and isn't a real
login.
However, running `systemctl --machine=traefik@.host --user` on our main "core"
user wouldn't work either.
While that way, systemd would, in theory, be able to connect to the user
session of traefik -- in this case it couldn't do so, since there doesn't
exist such a user session in the first place.

A user session is created by systemd on a *login* of a user.
We aren't logged in as traefik, and as such, not even magical systemctl flags
can help us.
Instead, we need to first manually start the `user@traefik` service to set up
the user session and create the said socket.

When switching users we then also need to set the environment variable to
the said socket:

    (core) $ user=traefik
    (core) $ sudo systemctl start user@$(id -u $user)
    (core) $ sudo -iu $user \
                   DBUS_SESSION_BUS_ADDRESS=unix:path/run/user/$(id -u $user)/bus
    (traefik) $ systemctl --user start ...
    (traefik) $ podman run --rm ...

We can now use `curl` to connect and query our API from a different system:

    $ curl http://example.com:8080/api/rawdata

Alternatively, run `curl` from the same system connecting to `localhost`, but
since `curl` isn't part of the CoreOS image, you'd need to use
[toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/)
to install and use it.

We can also use a browser to connect to http://example.com:8080 and are
presented with a nice dashboard!

Note: If you are using Firefox with HTTPS-only mode you *mustn't* accept the
self-signed certificate if Firefox prompts you so as the dashboard doesn't
run on https in the first place -- and accepting the certificate will just
result in a 404.
Instead, go to Settings, HTTPS-Only Mode, Manage Exceptions and enter
`http://example.com` (replace with your domain).
Now navigate to the same URL again (without the `s` in https) and select
"Continue to HTTP site".
This will not be an option if you already accepted the certificate, even if
you added the no-HTTPS-only exception!

## Traefik Configuration Files

The above command for running the container manually was adapted from
[Traefik's Quick Start Guide](https://doc.traefik.io/traefik/getting-started/quick-start/).
We used `-p x:y` to map any *incoming* request on *any* IP (0.0.0.0 for IPv4)
on a port $x$ to port $y$.
This means, the port $y$ is the one we need to configure for Traefik to listen
on -- luckily, this is the default.

The only Traefik configuration we made was to set `api.insecure` in order to
expose the dashboard on `:8080`, and to use the "Docker Provider".
In short, a provider is a mechanism for Traefik to discover "Services", where
a service may be the server running your Nextcloud instance or the like.
If Traefik is set up to use Docker as its provider, it will listen on
`/var/run/docker.sock` (mapped through `-v` to our per-user podman socket)
for other containers running in this Docker/Podman instance.

We cannot use this provider going forward anymore, though, since our other
services will be run by different users, thus not connecting to the same
socket.
Instead, we use the "File Provider", i.e., traditional configuration files
that will tell Traefik where to look (namely, to connect over localhost to
the Servers listening there).

This will be our first `traefik.toml`:

    [api]
      dashboard = true
      insecure = true

    [providers.file]
      directory = "/etc/traefik/dynamic"
      watch = true

    [entrypoints]
      [entrypoints.web]
        address = ":80"

We will ask Traefik to search the specified directory for configurations and
even watch it for changes!
If we will change anything regarding these services or even add one,
Traefik will automagically hook it up!
We also configured an additional "entrypoint".
Due to having set `dashboard = true` and `insecure = true`, we already
have an entrypoint on `:8080` that is configured and setup by Traefik itself.
Basically, an entrypoint is just telling Traefik to listen on this port
and eventually handle/route incoming traffic to a specific service using
our service configuration -- which, as we are using a file provider, will be
in another configuration file.

We store our dynamic configuration in a directory `dynamic` in our home dir
(we will map it using `-v` to `/etc/traefik/dynamic`).
This could be a simple configuration file for a webserver, called `blog.toml`:

    [http]
      [http.routers]
        [http.routers.blog]
          entrypoints = ["web"]
          rule = "Host(`blog.example.com`)"
          service = "blog@file"
      [[http.services.blog.loadbalancer.servers]]
          url = "http://10.0.2.2:8080/"

We use the `[http]` namespace since this is a HTTP service.
Traefik distinguishes TCP and UDP services but has extra bells 'n' whistles
for HTTP services and while we could setup our webserver as a TCP server, this
would be a bit overly complicated.

The `[http.routers]` namespace collects all routers for HTTP traffic and we will
add one called `blog` which is only activated if the incoming request comes
from the `blog` subdomain.

The loadbalancer array is a bit more tricky to understand but it should suffice
to know that Traefik can automatically balance the load of incoming requests
between multiple servers providing the same service and here we can collect
all those servers.
In our case its just one, the HTTP server running on `:8080` on localhost.
But... we wrote `10.0.2.2:8080` and not `127.0.0.1:8080`?

Indeed, while the server will listen on `localhost`, this is only `localhost`
*within its own container*.
Since we will have multiple containers, each for one service, we have
"multiple `localhost`s".
The IP `10.0.2.2` is the IP of the Slirp4NetNS router used by Podman
which "collects" all these `localhost`s under one umbrella.

Let's run Traefik again -- with our newly configured provider:

    podman run --rm \
               --security-opt label=disable \
               -v /var/srv/traefik/traefik.toml:/etc/traefik/traefik.toml:Z \
               -v /var/srv/traefik/dynamic:/etc/traefik/dynamic:Z \
               --network slirp4netns:allow_host_loopback=true
               -p 80:80 \
               -p 8080:8080 \
               traefik:v2.5

We mapped our configuration files to where Traefik expects them and we also
removed the command line arguments to Traefik.
This is, in fact, essential, as Traefik doesn't accept both, CLI arguments
*and* configuration options and will silently ignore one of them!

The most interesting line is the `--network` line where we allow our container
to access `10.0.2.2` as described earlier.
Otherwise the Slirp4NetNS software routing stack would forbid it from doing so
(for good reason!).

## Automating Traefik Startup

Now that we can startup an instance of Traefik, let's automate this task and
start it on bootup.

Luckily, Podman can simply generate systemd unit files for us from existing
containes!
To make them more useful, we should *name* our containers by adding
`--name`.

    podman create --rm \
                  --name traefik \
                  --security-opt label=disable \
                  -v /var/srv/traefik/traefik.toml:/etc/traefik/traefik.toml:Z \
                  -v /var/srv/traefik/dynamic:/etc/traefik/dynamic:Z \
                  --network slirp4netns:allow_host_loopback=true
                  -p 80:80 \
                  -p 8080:8080 \
                  traefik:v2.5

Observant readers will have noticed, that I also switched from `podman run`
to `podman create`.
This will merely create the container, but not run it, which is good because
we will use systemd to start it in just a second.

    podman generate systemd --new --name traefik > ~/.config/systemd/user/container-traefik.service

The `--new` flag creates a systemd unit that doesn't expect the container
to already exist (e.g., by being created using `create` beforehand) which may
be useful if we delete the container and still want systemd to start it up,
creating a new one.

We can now run our server:

    systemctl --user daemon-reload
    systemctl --user enable --now container-traefik

A bit unfortunately though, this container will only start when we login as
the `traefik` user, since it is a user-container started by a user-unit.
We can fix that using lingering, by executing as root:

    # loginctl enable-linger traefik

## HTTP-Server

For our HTTP-Server hosting the "blog" we mostly replicate our work done
earlier. As root:

    # loginctl enable-linger httpd

As the httpd user:

    podman create --rm \
                  --name httpd \
                  --security-opt label=disable \
                  -v /var/srv/httpd/public-html:/usr/local/apache2/htdocs:Z \
                  -p 8080:80 \
                  httpd:2.4
    podman generate systemd --new --name httpd > ~/.config/systemd/user/container-httpd.service
    systemctl --user daemon-reload
    systemctl --user enable --now container-httpd

## Let's Encrypt

Using plain HTTP ain't the real deal anymore,
so we will now change our setup to use SSL/TLS and port $443$ instead.
The great news is that we don't need to adjust *anything* on any of our
servers (which is just httpd for now, oh well).
Since Traefik is a reverse proxy, it layers TLS around our traffic to the host,
the only unencrypted traffic goes from Traefik to httpd (i.e., contained
within our system).

For the `traefik.toml` we simply add another entrypoint called "websecure".
We also remove the insecure API access since we will be rerouting it behind
TLS as well.

We also setup Let's Encrypt using
[ACME](https://en.wikipedia.org/wiki/Automated_Certificate_Management_Environment)
to automatically generate TLS certificates, and an internal `acme.json`
file that we will store in `~/letsencrypt/acme.json`.
You need to replace the contact email as well obviously.

The most tricky part is the ACME challenge -- and this is something you have to
do yourself, as the choice is yours and it doesn't help much to show my
configuration.
The most easy-to-setup is probably the `httpChallenge` but you can use
DNS records to certify that you are indeed the owner of the domain as well.
Refer to
[Different ACME Challenges](https://doc.traefik.io/traefik/https/acme/#the-different-acme-challenges)
for more information.


    [api]
      dashboard = true
      #insecure = true

    [providers.file]
      directory = "/etc/traefik/dynamic"
      watch = true

    [entrypoints]
      [entrypoints.web]
        address = ":80"
      [entrypoints.websecure]
        address = ":443"

    [certificatesResolvers.letsencrypt.acme]
      email = "exampleuser@example.com"
      storage = "/letsencrypt/acme.json"
      [certificatesResolvers.letsencrypt.acme.xxxxxxxChallenge]
        # ???

Now we restart Traefik and this is it -- almost.

    systemctl --user restart container-traefik

This does indeed setup TLS for Traefik and in the logs you will probably notice
that Traefik is waiting for Let's Encrypt to provide you with the certificate
(this process can take a while).

However, all our services (only httpd) listen on `:80`.
Luckily, we can change that without restarting Traefik!

    [http]
      [http.routers]
        [http.routers.blog]
          entrypoints = ["websecure"]
          rule = "Host(`blog.example.com`)"
          service = "blog@file"
          [http.routers.blog.tls]
            certresolver = "letsencrypt"
            domains = [{main = "*.example.com" }]
      [[http.services.blog.loadbalancer.servers]]
          url = "http://10.0.2.2:8080/"

We change our entrypoint from "web" to "websecure" and also configure that
our router is actually using the configured certresolver from the main
`traefik.toml` file -- done.

However, we cannot access our dashboard anymore, let's change that as well
by creating a `dashboard.toml` next to the `traefik.toml`:

    [http]
      [http.routers]
        [http.routers.dashboard]
          entrypoints = ["websecure"]
          rule = "Host(`traefik.example.com`) && (PathPrefix(`/api`) || PathPrefix(`/dashboard`))"
          service = "api@internal"
          [http.routers.dashboard.tls]
            certresolver = "letsencrypt"
            domains = [{main = "*.example.com" } ]

We use a more complex rule to only match `traefik.example.com/api` and
`traefik.example.com/dashboard`, everything else will be routed into the void
(or, at least not matched by *this* router).
The service behind this route is already provided for by Traefik as
`api@internal`, which is also why we don't need to configure a server IP.

Accessing https://traefik.example.com/dashboard/ (the trailing `/` is important)
will allow us to access our dashboard!
If we want, we can also setup simple user-password authentication using a
middleware:

    [http]
      [http.routers]
        [http.routers.dashboard]
          entrypoints = ["websecure"]
          rule = "Host(`traefik.example.com`) && (PathPrefix(`/api`) || PathPrefix(`/dashboard`))"
          service = "api@internal"
          middlewares = ["auth"]
          [http.middlewares.auth.basicAuth]
            users = [
              "exampleuser:$2y$05$lLmSqHB81HfGoXVcC0VJkeNPoFamd0736S3jzwmErLKdLkNK.LWLG",
            ]
          [http.routers.dashboard.tls]
            certresolver = "letsencrypt"
            domains = [{main = "*.example.com" } ]

The `basicAuth` middleware can use bcrypt encrypted password hashes that we can
create using `htpasswd`, e.g.:

    htpasswd -nB exampleuser
    New password: ••••••
    Re-type new password: ••••••
    exampleuser:$2y$05$lLmSqHB81HfGoXVcC0VJkeNPoFamd0736S3jzwmErLKdLkNK.LWLG


## Wrapping up

It might also be sensible to have automatic rerouting from HTTP to HTTPS.
We can set this up using the following configuration:

    [http]
      [http.middlewares]
        [http.middlewares.redirect-to-https.redirectscheme]
          scheme = "https"
      [http.routers]
        [http.routers.http-catchall]
          rule = "HostRegexp(`example.com`, `{subdomain:[a-z0-9-]+}.example.com`)"
          entryPoints = ["web"]
          middlewares = ["redirect-to-https@file"]
          service = "dummy@file"
      [http.services]
        [[http.services.dummy.loadBalancer.servers]]

While we've now written three of these drop-in dynamic configurations,
internally, Traefik merges them to something like:

    [http]
      [http.routers]
        [http.routers.blog]
          entrypoints = ["websecure"]
          rule = "Host(`blog.example.com`)"
          service = "blog@file"
          [http.routers.blog.tls]
            certresolver = "letsencrypt"
            domains = [{main = "*.example.com" }]
      
        [http.routers.dashboard]
          entrypoints = ["websecure"]
          rule = "Host(`traefik.example.com`) && (PathPrefix(`/api`) || PathPrefix(`/dashboard`))"
          service = "api@internal"
          middlewares = ["auth"]
          [http.middlewares.auth.basicAuth]
            users = [
              "exampleuser:$2y$05$lLmSqHB81HfGoXVcC0VJkeNPoFamd0736S3jzwmErLKdLkNK.LWLG",
            ]
          [http.routers.dashboard.tls]
            certresolver = "letsencrypt"
            domains = [{main = "*.example.com" } ]
            
        [http.routers.http-catchall]
          rule = "HostRegexp(`example.com`, `{subdomain:[a-z0-9-]+}.example.com`)"
          entryPoints = ["web"]
          middlewares = ["redirect-to-https@file"]
          service = "dummy@file"
          
      [http.middlewares]
        [http.middlewares.redirect-to-https.redirectscheme]
          scheme = "https"
          
      [http.services]
        [[http.services.dummy.loadBalancer.servers]]
        [[http.services.blog.loadbalancer.servers]]
          url = "http://10.0.2.2:8080/"

Having them split up into single configuration files is much more
comprehensible though!


[^1]: Note that the application within the container can still run as an
      unprivileged user process, however if there's a problem in the container
      engine this process may take over the container itself.
