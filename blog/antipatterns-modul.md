---
title: Antipatterns Of Modularization
date: TO-BE-FILLED-BY-OEM
author:
- ljrk
keywords: [programming, C, software engineering, code style, modularization]
---

## The Endless-Loop Function

* `bar` is badly named, should be `bar_loop` or similar
* However, if `foo` & `bar` are short, inlining it may just be
  the better idea.

    int bar(void)
    {
    	while (1) {
    		/* ... some code ... */
    	}
    }

    int foo(void)
    {
    	/* ... some code .. */
    	
    	// endless loop
    	bar();
    }
