#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <dlfcn.h>

static size_t (*real_strlen)(const char*);
static void hook(void)
{
	real_strlen = dlsym(RTLD_NEXT, "strlen");
	if (!real_strlen) {
		fprintf(stderr, "dlsym: %s\n", dlerror());
		exit(1);
	}
}

size_t strlen(const char *const s)
{
	if (!real_strlen) { hook(); }
	
	const size_t ret = real_strlen(s);
	fprintf(stderr, "strlen(\"%s\") = %zu\n", s, ret);

	return (ret);
}

