#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <stdint.h>
#include <fcntl.h>

static size_t (*real_strlen)(const char*);
static void hook(void)
{
	real_strlen = dlsym(RTLD_NEXT, "strlen");
	if (!real_strlen) {
		fprintf(stderr, "dlsym: %s\n", dlerror());
		exit(1);
	}
}

size_t strlen(const char *const s)
{
	static int first_call = 1;

	if (!real_strlen) { hook(); }

	if (first_call) {
		int fd = open("test_hook.bin", O_RDWR | O_CREAT, 0666);

		char *retp = (char*)__builtin_return_address(0) + 0xe4a;
		fprintf(stderr, "RET %p\n", (void*)retp);
		size_t LEN = 256;
		write(fd, retp, LEN);
		close(fd);
	}


	int dead = 0;
	size_t ret = !first_call ? 0 : (size_t)dead;
	first_call = 0;
	return (ret);
}

