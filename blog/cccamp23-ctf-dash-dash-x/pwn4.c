#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <dlfcn.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>

static size_t (*real_strlen)(const char*);
static void hook(void)
{
	real_strlen = dlsym(RTLD_NEXT, "strlen");
	if (!real_strlen) {
		fprintf(stderr, "dlsym: %s\n", dlerror());
		exit(1);
	}
}

size_t strlen(const char *const s)
{
	(void)s;

	static int first_call = 1;

	if (!real_strlen) { hook(); }

	if (first_call) {
		const int fd = open("test_hook.bin", O_RDWR | O_CREAT, 0666);
		if (fd < 0) {
			fprintf(stderr, "open: test_hook.bin: %s\n", strerror(errno));
			exit(1);
		}

		const char *const test_hook = (char*)__builtin_return_address(0) - 0x20;
		fprintf(stderr, "test_hook: approx %p\n", (const void*)test_hook);
		assert(write(fd, test_hook, 4096) == 4096);
		close(fd);
	}

	first_call = 0;

	return (0);
}

