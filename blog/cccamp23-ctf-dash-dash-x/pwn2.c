#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <stdint.h>
#include <fcntl.h>

static size_t (*real_strlen)(const char*);
static void hook(void)
{
	real_strlen = dlsym(RTLD_NEXT, "strlen");
	if (!real_strlen) {
		fprintf(stderr, "dlsym: %s\n", dlerror());
		exit(1);
	}
}

size_t strlen(const char *const s)
{
	static int first_call = 1;

	if (!real_strlen) { hook(); }

	int dead = -1;
	size_t ret = first_call ? 0 : (size_t)dead;
	fprintf(stderr, "strlen(\"%s\") = %zu (%d)\n", s, ret, (int)ret);

	first_call = 0;
	return (ret);
}

