---
title: Computing and Security Wishlist (Permanently Updated)
date: 2021-12-26
author:
- ljrk
keywords: [unix, linux, security, project, ideas]
---

I have many ideas for projects, some of which I start actually implementing,
but even then, often abandoning later-on for various reasons.
But I also forget many of these ideas and so this shall be a list of projects
that I may implement, but I'm mostly hoping for someone else to solve :-)

# PGP-less Yubikey

Right now my setup for my YubiKey is to use it's OpenPGP smartcard feature
to encrypt my passwords stored & synced using `pass(1)`, more details
on this [here](yubikey.html).

However, using PGP just for that is using a (large, unwieldly) hammer for
something rather simple.
The `age(1)` tool is much better suited for that and could be used instead.
Alternatively, the other smartcard backend of YubiKeys/SolokeysV3 could be
leveraged as well.

Both is kind-of WIP

* https://github.com/android-password-store/Android-Password-Store/issues/1486
* https://twitter.com/FiloSottile/status/1469041023196221444

So I'm happy to lean back and observe what's happening.

# A Worthy Successor For SunRay -- The Not-Cloud Portable Workstation

## Silverblue + homed

## Flatpak Issues

## homed+fs-volumes+encryption

btrfs doesn't support this

## Stratis

## sd-boot with coreboot+heads

## TPM-backed host-credentials

Strict separation of per-host and per-user credentials.

See LeonardKoenig.gitlab.io/website.mkiv

# RICOH SP 150 PAPPL application

# E2E Matrix with Telegram feature parity

# Matrix for federated SSO

# A POSIX-only system

# E-Ink Wireless Display

# Remarkable 2 Open Distribution

# sane-airscan + scanbd

# John Lion's Commentary on UNIX Version 6 Restoration

# Open Interactive low-level programming toolkit

# Singstar -> Ultrastar -> ASCII Star -> ???

# Rust CC

# Typed Shell

# macOS virgl/virvulkan backend

# Open Streaming Platform

# Open Food Ordering

# ConTeXt + JATS XML

# Tectonic LuaTeX for ConTeXt

# Mastodon + Twitter client

# Ledge/Beancount with Bank support

# Federated Zulip + Zulip-Mail bridge

# Separate Bookmarking & Sync out of Browsers

# Browser Keyword Search

# Storing GNOME display configurations

# DSA Hero Generators

# BBC calendar

# Synchronized Episode Watch Tracker

-> actually completely decentralized user data.

# Cooking Recipe Format, Sync, Sharing and Printing

# Steam Install Games as Flatpak

# Newsfeed

# Editor.
