---
title: Hitchhiker's Guide To The Fediverse
date: 2024-07-28
author:
- ljrk
keywords: [fediverse, mastodon, federation, social networks, moderation, Twitter, X, Threads, BlueSky, ActivityPub]
---

# What is the Fediverse and Why is it Interesting?

Most people have heard of "Mastodon";
some of them were probably already berated that actually they mean the Fediverse.
Indeed, to understand why we maybe want to use Mastodon
(or, *cough*, the Fediverse)
requires a short introduction into the difference between these terms
and why we should prefer saying "The Fedi(verse)":

## De-Centralised Social-Media

The Fediverse offers an alternative to big "centralised" social-media *platforms* such as Twitter/X.
Those are controlled by one entity,
making for a unified and controlled experience of human interaction.
There, moderation cannot exist without censorship
and is crucially dependent on the goodwill of the platform owner.

In contrast, the fediverse approaches social-media more akin to how e-mail used to be:
Different e-mail providers become "fediverse servers/instances",
and they all may differ in their Terms and Conditions.
And yet, users from GoogleMail can connect with Yahoo or personal hosted servers.
Also, the web interface of GoogleMail differs from Yahoo
but both can be used with a dedicated e-mail client such as Thunderbird or Outlook
for a more unified experience.
Mastodon, specifically mastodon.social, is one of those "instances".
But since it is powered by open-source software and allows for federation,
there are many "look-alike" instances such as "todon.eu" hosted by different people.
It's the same software powering it, but with different rules.
Yet, I can talk to people on mastodon.social and vice-versa.

The similarity goes further in that both e-mail addresses and fediverse handles
consist of a username and a server name, separated by an at-symbol.
For the fedi, an additional at-symbol in front is used to distinguish a fedi handle from
an e-mail address, e.g., "@ljrk@todon.eu".

Especially with the recent downfall of Twitter and its ownership,
but also when looking at how most centralized platforms (LinkedIn, YouTube, ...)
fail at handling moderation (some call it "too little",
others want more/stricter moderation) different approaches become interesting.
The situation gets even more important with legislation and regulation around Internet communications
such as the German NetzDG.

Before we go into how to best join the Fediverse,
let's discuss in short how the Fediverse approaches moderation
and why this helps dealing with the above issues.
Feel free to skip this section although it provides reasoning for my recommendations.

## On Moderation and Social Circles

Making moderation fair while keeping speech best protected
*and* ensuring that vulnerable groups are protected seems like and impossible task.
This is worsened the bigger a circle grows because needs and perspectives of groups get more diverse,
while the possible number of interactions (and thus need for moderation) increases exponentially.
Most platforms meet this using automated processes with
AI, vote-like systems for flagging,
with only minimal human staff to do basic error correction.
There's little room for nuance,
especially with the many cases where all sides involved have certain legitimate points.

However, making circles smaller and simply foregoing the goal (for now) of universal interaction between arbitrary people immensely decreases moderation cost.
This suddenly makes it possible for small teams to "run" a social circle,
moderate it and often resolve conflict even harmonically.
Also, it distributes the workload to the many small moderation teams of all the social circles.
Indeed, often there doesn't really need to be "one" moderation team
but everyone takes care of each other and sometimes steps in,
if conflict gets too heated.
This is how friend groups work in real life.

Each Fediverse instance copies this approach, somewhat, by providing such a "home" for all its users.
Different instances may have different house rules that may stand in conflict to each other.
But users are free to make new friends and join other groups.

At the same time, friend groups do sometimes intersect and interact.
This is often not a deep collusion where completely opposite ideals clash,
but usually initiated by certain overlaps in interests between single members of both groups.
This induces some sort of initial mutual respect and also ensures that there's *some* common ground,
and that differences are likely not too big to overcome.
As it happens, sometimes indirect interactions where one friend group effectively acts as a conductor can occur as well.

In the end, virtually every human being is *somewhat* connected to everyone else,
through some path of friends or acquaintances.
This allows us to relate to others and even maybe accept a person with quite a different opinion than ours;
simply because "that's my best friends mom, she's a good person at heart, though".

And this, to a certain degree, is what federation between Fediverse instances uses to ensure fair moderation,
without infringing on everyone's freedom:
Whilst different instances may have different rules
(i.e., the same post may have been allowed on one, but not the other server),
they may still *federate*.
That is, users from the other server can still comment and even like or "boost" (retweet/retoot/reblog/...) that post.
Depending on the rules, "boosting" such content may be disallowed on their instance of course,
but that is *their* issue, not the issue of the original poster.

In reality, this means that certain posts and posters
(which are *egregiously* out of line with your instance's rules)
may not be visible for you.
Still, they are free to see on their instance and those that didn't delete them.
There's no censorship of the poster going on,
only those who declared that they do not want to see such content won't.

The end result is that posters don't need to try to fit into "everybody's" individual rule set and maybe even self-censor,
but only their home instances one.
Further, instance rules can be much more specific and strict than those platforms that try to cater to every group of people.
This makes moderation more easy as well as knowing whether your own post is in violation of the rules,
meaning that moderation actions are much more predictable and less surprising if they do indeed happen.
If your post violated the rules, you probably know that.
And if you don't like the moderation decision,
you probably don't really like the rules and should've chosen a different server.
And this brings us to the next step -- onboarding the Fedi :-)

# Fediverse Onboarding

The most crucial step is selecting a server/instance to join.
Servers differ in size (which is *not* the number of people you can communicate with!),
the software used (e.g., Mastodon), and the house rules.
The latter are probably the most important,
the size the least.

## Chosing a Server

Before we actually look into what kind of rules you'd be happy with,
shortly consider the question for the server software.
While this sounds like a very technical criterion (and it can be),
it has quite some implications.
While the most famous Fediverse server software, Mastodon, is quite closely modeled after Twitter/X-style
"micro-blogging",
there are others.
Such as Pixelfed which is inspired by Instagram, or WriteFreely which is closer to a "macro-blog" style posting,
and lemmy or kbin, which take inspiration from link-aggegators like Reddit and discussion/image boards or forums.
There's also PeerTube, a federated YouTube alternative and even more specific servers like
FunkWhale (audio streaming), BookWyrm (Amazon GoodReads alternative), Mobilizon (event/activism organization).

### eXodus/Twexit: Micro-Blogging Alternatives

For micro-blogging (which I will focus on), there are other more playful alternatives to Mastodon,
that add various kinds of functionality that may not be crucially essential but can definitely add a lot of fun:
Animated reactions, rich and animated text posts or a plugin system.
The most common alternative to Mastodon are the various forks of the Misskey server,
Sharkey, Calckey, Firefish, etc. (often together called forkey).
If all this sounds too much, choose Mastodon and scroll through
[Join Mastodon](https://joinmastodon.org/servers);
or ignore this question entirely and simply look for a server that matches your taste starting at
[JoinFediverse](https://joinfediverse.wiki/Instances), or
[Fediverse Party Server List](https://fediverse.party/en/portal/servers/), or
[Fediverse Observer Server List](https://fediverse.observer/list).

### House Rules

When looking for a server, look at its rules.
I would advice against servers that are rather "catch-all" or unspecific such as "discrimination not allowed".
Instead, more specific rules such as "misgendering is not allowed" or "we take free speach literally" (if that's up your jam) are much more 
effective at communicating what to expect.
Both in terms of content you will see, as we well as posts you're allowed to write.
They serve as the best vibe-check you will ever get.

### Size

Finally, the size.
Simply put: The larger the server, the more difficult moderation will be and at some point,
such a server *effectively* ends up suffering the same problems as centralized platforms.
Mastodon.Social, Threads.Net (by Facebook), Vivaldi.Social, Mozilla.Social, all suffer from this problem to some degree.
Since they all do not aim for a specific target group with somewhat of a shared ideology,
moderation decisions are hard and will, at some point, feel either arbitrary or over the line.

Further, some servers completely de-federate with especially Threads.Net,
because of exactly that issue:
Whilst moderation on an instance can hide certain posts locally,
bigger instances that are either unmoderated or completely unhinged (Trump's Truth.Social is also Mastodon!)
are effectively un-moderatable for another instance and thus completely cut-off from the rest of the Fediverse.
You won't be able to communicate with people on todon.eu (like me) with either Truth.Social or Threads.Net.

## Creating an Account

If you've found an instance you want to onboard,
account-creation itself is quite straight-forward.
In most communities and across the Fedi in general,
it's quite well-received to post an #intro post with a broad introduction of yourself.
Why are you here, what to do you expect/want, what are your interests, etc.
This post can be boosted by people in turn following you,
quickly expanding your visibility and follows and possibly helping you find people you wanna follow, too!
Also, be sure to fill in your biography (e.g., website/blog, pronouns, ...) and set a profile-picture.

## First Weeks

Most Fediverse servers do not use complex algorithms to match your interests and recommend you accounts.
This is both a blessing and a curse:
Your feed will probably be quite empty at first, but also there won't be an Elon Musk popping into it, completely uncalled-for.
To counteract this, follow liberally -- you can always unfollow later!
Found a post you find cool or interesting, just follow.
You may even resort to importing follow-lists for certain topics (e.g., I imported follow lists of infosec people).
Also be sure to look at your friends/mutuals follows:
They tend to follow people with similar interests,
maybe use a tool such as [Fedigraph for Mastodon](https://followgraph.vercel.app/) to discover them.
I'm following ~750 people and there's wayyyyy enough cool stuff :)

## Choosing a Client

If your instance doesn't use any of the Forkeys and instead Mastodon, the default web interface pretty much sucks.
The same goes for the "official" app.
I highly recommend using a different client, such as
Ivory (made by Tapbots, of Tweetbot for Twitter fame) or Ice Cubes on iOS,
Moshidon/Megalodon on Android,
or simply Phanpy.Social or Semaphore.Social on the Web (works on mobile, too).

# (Almost) Final Words

My personal account is @ljrk@todon.eu on an instance that's rather radical left,
explicitly disallowing Tankies (Stalinists etc.) and Nazis and their content,
but also moderating pro-capitalist propaganda.
They heavily moderate anything transphobic and usually when reporting such content,
they were deleted before I the content loaded after clicking the notification :-)

This instance is not for everyone.
It's an activist instance and I do not share all the views of the people there.
But, for me, the discussions there are healthy and more likely discussions I'd like to have than others,
and the moderation keeps Nazis and transphobes far away from me.

Similarly, for companies and organizations, topical instances make sense too.
The company I'm employed at is on @infosec.social, many others found their way there, too.
@hachyderm.io is also quite popular, as is @journa.host or geographically local instances (if it makes sense).

# On BlueSky

BlueSky is a different federated social network, incompatible with the ActivityPub based Fediverse.
It takes different approach to moderation,
removing the power of server instances and instead putting it to each individual.
As such, there's no such thing as instance wide blocks or de-federation:
You can access each and every post.

While great for literal free speech, I don't think that this is a good approach for a multitude of reasons.
First, this puts the burden on the individual, and depending on your privileges, this is quite a burden indeed.
Right now, BlueSky isn't *that* bad, simply because of quite a select user base.

Second, I think that modeling protocols and services for digital human interaction on how we humans do interact in real life is a good idea indeed.
I consider the power of instance moderators a service with quite some merit -- similar to my friends around me:
They do not only protect me, but also help as a good sanity-check and serve as a self-help peer group.
