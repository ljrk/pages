---
title: Realforce R3 Specifications and Buyer's Guide
date: 2023-10-19
author:
- ljrk
keywords: [keyboards, jis, ansi, office, tkl, bluetooth, topre]
---

The Japanese vendor Realforce released their new top-line model the Realforce R3 quite some time ago.
Unfortunately, information about this keyboard is rare, incomplete or hard to understand.
I've now tried to compile my own overview of the changes and new models,
mostly based on the information from an article by GREENKEYS [^1].

Additionally, outside of Japan this seems to be pretty hard to acquire,
but I (Europe/Germany) could successfully order from akiba [^2].

Finally, it's quite hard to come across an English version of the manual,
which is why I included a scan of my copy
[here](https://ljrk.codeberg.page/blog/realforce_r3/Realforce_R3_manual_EN.pdf).

# Changes

The R3 line introduces some technological changes in all available models, such as

 * More flexibility when remapping keys in firmware
 * The Actuation Point Changer (APC) gains a 0.8mm option

Additionally, there's a new design

 * with a more rounded board
 * a new button that can switch between two firmware-stored layouts as well as enable a "simple mode" with no NKRO etc. for compatibility problems
 * sublimation printed keycaps which are more resistant to wear than the previous laser printing

For those preferring the older design, there's also a variant incorporating the technical advancements.
Do note, however, that this model is not available for macOS and also only in a wired version.

# Models

The different models have model numbers matching this regex:
```
R3[USH][ABCDEFGH][1-4][1-3]
   sty  layout    col  loa
```

In order to help with the choice for **sty**le and wired/wireless, **layout**, **col**or and loudness, as well as **loa**d,
I've created a list of questions you should answer for yourself.

## Decision Tree

 1. Choose your layout (layout)
    i. Region ANSI (B, D, F, H) vs. JIS (A, C, E, G)
    ii. The keyboard size TKL (C, D, G, H) vs. Full (A, B, E, F)
 2. Your keys (col)
    i. Between silent (1, 2) vs. standard (3, 4)
    ii. And the keyboard color black (1, 3) vs. white (2, 4)
 3. The platform (layout,  again)
    * For mac (E--H) this ends here as the only option is to have 45g key pressure,  and the R3 style design with hybrid connectivity
    * For PC (A--D) the choice continues...
 4. The key pressure (load): 45g (1) vs. variable (2) vs. 30g (3)
 5. They overall style (style)
    * For R2 style (S) wired is the only option
    * Otherwise you can choose between Wired (U) and Hybrid (S)

I hope that helps deciding which model is right for you!

If you are given a model number and want to decode it,
or simply look for a for a full overview of all model indicators and their meanings, read on!

## Full List of Options

sty(le) + wireless:

 * U: R3 Style Wired (Win only)
 * H: R3 Style Hybrid
 * S: R2 Style Wired (Win only, no R2 hybrid option)

layout:

 * A: Win Full JIS
 * B: Win Full ANSI
 * C: Win TKL JIS
 * D: Win TKL ANSI
 * E: mac Full JIS
 * F: mac Full ANSI
 * G: mac TKL JIS
 * H: mac TKL ANSI

col(or) & loudness:

 * 1: silent black
 * 2: silent white
 * 3: standard black
 * 4: standard white

loa(d):

 * 1: 45g
 * 2: variable (Win only)
 * 3: 30g (Win only)


[^1]: https://green-keys.info/en/what-is-the-difference-between-realforce-r3-and-r3s-and-how-to-choose-the-right-type-of-realforce/
[^2]: https://akibashipping.com/collections/vendors?q=%E2%80%8ETopre%20Corporation
