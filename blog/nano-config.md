---
title: Configuring nano for Windows users
date: 2021-07-24
author:
- ljrk
keywords: [unix, linux, programming, nano, console]
---

Often people who are using Linux (or BSD/Solaris/macOS for that matter)
on the console find themselves a bit hard to edit files in the terminal.
There are various choices for editors, from the standard editor ed(1),
vi(1), vim, emacs to kakoune, all with varying learning curves.

Increasingly many distributions switch the default installed editor to
nano however, due to the much better discoverability of the editor.
Still, however, the default keybindings of nano are, for people coming
from Windows, rather different than what users are used to.

For people using the shell regularly, it makes sense that nano, by
default, refrains from using keybinds such as Ctrl+S or Ctrl+Z and
Ctrl+C, Ctrl+V.
The editor does support this, with pretty easy configuration, so for
everyone who wants a Windows-y feel, here you go.

# The Configuration File

To configure nano, edit the config file which by default would be placed
in

    ~/.config/nano/nanorc

You can create the directory, if it doesn't exist and edit the config 
(with nano of coure) using:

    $ mkdir -p ~/.config/nano
    $ nano ~/.config/nano/nanorc

You can now disable the default functionality of Ctrl+Z (\^Z in UNIX
speak) by adding the following line:

    # Disable ^Z for suspending the editor
    unset suspendable

Now that we have \^Z free, we can bind it, as well as others, to their
respective common functions:

    # Rebind keys to a more "common" layout while editing text (main)
    bind ^Z undo main
    bind ^S writeout main
    bind ^F whereis main
    bind ^X cut main
    bind ^V paste main
    bind ^R replace main

This will bind \^Z to "undo" when we're editing text, i.e., in the
"main window".
We can place nano in other menus/windows where we don't edit files
and want to retain any funcitonality of \^Z there (if there is some).

However, for quitting, we want to bind \^Q, regardless of the
window/menu we are currently in:

    # ... and in other "menus" as well
    bind ^Q exit all

Finally we may add displaying line numbers:

    set linenumbers

More details on the available settings can be found if you open
the man(ual) page for the configuration file:

    $ man nanorc

Under the heading **OPTIONS** you'll find the different, well, options
available.
The possible functions for keybinds are under section **REBINDING
KEYS**.
Nano is incredible configurable and astoundingly powerful, so if
something bothers you, change it.

Or switch to a better editor, like kakoune <3