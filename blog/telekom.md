---
title: Offene Beschwerde Telekom
date: 2022-03-25
author:
- ljrk
---

# Das Problem 

Ich habe über die letzten 3 Tage versucht mein -- zugegeben sehr technisches -- Problem über den Support zu lösen.  Um Worte zu sparen verlinke ich mein Problem aus dem Forum:
[VLAN/QoS Werte bei Einwahl in Telekom Netz via eigenen Router](https://telekomhilft.telekom.de/t5/Festnetz-Internet/VLAN-QoS-Werte-bei-Einwahl-in-Telekom-Netz-via-eigenen-Router/m-p/5630196/highlight/true#M88237).

# Hergang

Ich habe als am Mi Abend versucht über den Support diese Frage(n) zu klären, aber am Ende wurde mir erklärt, die technischen Experten seien schon nach Hause gegangen.  Am nächsten morgen um 8 habe ich dann gleich angerufen und mir wurde ein Rückruf für gestern um 9 versprochen.  Dies ist nicht geschehen.

Ich habe dann selber nochmals angerufen und wurde zum technischen Support geleitet.  Mit der Ausnahme eines Mitarbeiters waren alle 5(!) anderen Mitarbeiter nicht hilfreich:

1. Ein Mitarbeiter beharrte darauf, dass die Zugangsdaten für die PPPoE-Einwahl ausreichend seien -- wider des von Ihnen veröffentlichten Technischen Reports 1TR112 (siehe Forumpost) wo detailliert ist, welche anderen Einstellungen notwendig sind.  Dieser Mitarbeiter war darüberhinaus auch höchst unfreundlich und zeigte keinerlei Verständnis für mein Anliegen.
2. Ein weiterer Mitarbeiter hat versucht mir einen Zusatzvertrag beim Digital Homeservice aufzuschwatzen... ich zahle doch nicht mehr nur weil die Telekom ihre Technik nicht auf die Reihe bekommt?!
3. Alle weiteren Mitarbeiter konnte alleine schon mit den Begriffen in der Frage nichts anfangen und wussten z.T. auch nicht, dass der Speedport 4 als reines DSL-Modem betrieben werden kann (das steht in der *sehr kurzen* Bedienungsanleitung!).
4. Grundsätzlich wurde mir dann gesagt, dass der Entstörungsdienst nicht entsprechend geschult ist und ich es aber einfach nochmal probieren kann und vllt. werde ich an jemanden weitergeleitet der "zufällig" doch davon Ahnung hat.  Nach 2 Stunden Telefonierei ist das blanker Hohn.
5. Final wurde mir dann ein weiterer Rückruf versprochen, für heute 9 Uhr.  Auch das ist (ich war nicht überrascht) nicht passiert.

Ich habe nun also nochmal selber angerufen und wurde erst, erneut, an einen technischen Mitarbeiter weitergeleitet, der mir sagen wollte, dass das ja ein Problem meines Heimnetzes sei (nein, es geht ums WAN, nicht LAN!).  Ich habe dann einfach gefragt, warum man mir denn verspricht mich zurückzurufen und das dann einfach nicht passiert -- dann wurde ich weggedrückt.

Im nächsten Anruf sollte ich mit jemandem verbunden werden der "Ahnung" hat, wurde also zurück in die Warteschleife gepackt und dann bekam ich nach 5min einen Fehler "Diese Rufnummer ist uns nicht bekannt oder wird nicht unterstützt" und die Verbindung brach weg.

Der letzte Anruf war dann der abschließende, und auch beste.  Ich habe gar nicht erst darum gebeten zum technischen Support weitergeleitet zu werden sondern nur gefragt, ob es denn wirklich keinen "Weg" gibt für technische Probleme die zwar im Aufgabenbereich des technischen Supports liegen aber von diesen nicht geklärt werden können.  Das wurde mir, schockierend wie gleichermaßen überraschend, bestätigt.

# Fazit

Ich finde diese Art mit Kunden umzugehen kurz gesagt dreist.  Ich verstehe, dass nicht die einzelnen Berater hier "das Problem" sind.  Vielmehr ist es die Struktur die nicht den Fall vorsieht, dass der technische Support nicht weiter weiß.  Und manche verteidigen im Internet noch die Telekom mit "naja, da kriegt man immerhin guten Service."  Da habe ich bei Kabel Deutschland(!) besseren Support bekommen -- und die sind berüchtigt.
