---
title: Solingen, ein Erfolg der Rechten.
date: 2024-09-10
author:
- ljrk
keywords: [politics, terrorism, racism, nationalism, germany]
language: de-DE
---

Seit ca. einer Woche beschäftigt mich das Statement von Robert Habeck zum Attentat in Solingen[^1].
Als ich das Video das erste Mal gesehen hab, haben mich seine Aussagen sehr betroffen gemacht.
An dieser Betroffenheit ist seit dem nichts verblasst.

Denn im Gegensatz zu vielen die seine Ansprache als "unpopulistisch" und "ausgewogen" lob(t)en,
bin ich schockiert über den offenbar tief sitzenden Rassismus der hinter diversen Aussagen steckt
und die absolut unsinnig hilflosen Lösungsvorschläge:

In dem Video stellt Habeck, nach einigen -- sehr guten! -- Worten des Mitgefühls und der Einordnung,
seine drei Punkte vor:

1. Ausbau der Fähigkeiten der Polizei, insbesondere um frühzeitig Gefährder zu erkennen.
2. Strengere Messerverbote und -kontrollen.
3. Menschen die unter Schutz des Asyls stehen abschieben, wenn sie Gewalttaten verüben.

Gehen wir diese Punkte einzeln durch.
Habeck schlägt vor von einem Rechtsstaat sich wegzuentwickeln,
den Polizeistaat- und Überwachungsstaat direkt zu überspringen und zu einem "Präventionsstaat" zu werden.
Jedenfalls ist das das Resultat seiner Forderung "Gefährder erkennen".
Es geht nicht mehr darum, Gesetzesbrüche zu ahnden sondern zu verhindern --
und zwar über polizeiliche Mittel.
Hierfür müsste ein enormer Ausbau der Überwachungskapazitäten vorgenommen werden.
Inclusive der Konsequenzen für unsere Freiheit, insb. derer, von Menschen,
die ohnehin schon von Polizeigewalt oder politischer Repression betroffen sind.

Ich bin mir recht sicher, dass dies nicht sein Ziel ist,
aber am Ende verspricht er einen "guten" Präventionsstaat.
Diesen gibt es jedoch nicht.
In einem Rechtsstaat müssen wir uns damit abfinden,
dass Kriminalität existiert.
Wir können nicht in die Köpfe von Menschen reinschauen
(und selbst wenn, wir sollten es nicht)
oder durch vermeintliches "Gefährderverhalten" bereits
im Voraus Menschen in Gewahrsam nehmen.
Das Versprechen einen freiheitlichen Staat zu schützen,
indem man Gewalt verhindert ist utopisch.

Aber genau dieses Versprechen geben uns die Rechten:
Sie werfen sich auf jedes Attentat (solange es in die Narrative passt, dazu später mehr),
jede Kleinstnachricht und zeigt mit dem Finger auf die Politik:
Die hat das nicht verhindert, sie sind unfähig, wir leben alle in Unsicherheit!

Dabei sollte ein Rechtsstaat -- und das erwarte ich auch vom Vizekanzler --
mit einem gewissen Stolz verkünden:
Nein, trotz dieses Terroranschlags bleiben wir ein freiheitliches Land,
wir werden uns nicht durch diese Provokation unsere Freiheit nehmen lassen.
Wir halten das aus -- so sehr es schmerzt.

Habeck's zweiter Punkt schlägt in die gleiche Kerbe:
Mehr Möglichkeiten geben für die Polizei frühzeitig zu intervenieren,
indem auch schon kleinere Messer verboten werden würden.
Dabei ist der Anschlag mit einer bereits "verboten langen" Klinge ausgeführt worden,
eine solche Einschränkung hätte also keinen Unterschied gemacht.
Allenfalls eine ständige Kontrolle von Taschen durch die Polizei,
wobei sich da die Frage stellt:
Wollen wir das wirklich?

Der dritte Punkt ist aber der, der mich am Meisten getroffen hat.
Weil er nicht nur naive Versprechungen mit Maßnahmen die gegen die Freiheit Aller gehen
kombiniert hat,
sondern am Ende des Tages rassistische Ressentiments und vor Allem Maßnahmen befeuert.
Insgesamt sind die Mordzahlen seit Jahrzehnten radikal gesunken und auf einem niedrigeren
Potential als noch in den 90ern.
Und da haben sich Leute nicht wegen der ganzen "Messermänner" unsicher gefühlt.
Ja, in den letzten Jahren sind die Zahlen wieder leicht gestiegen,
aber in einem Bereich geblieben der weiterhin akzeptabel ist,
akzeptabel sein *muss* -- für einen Rechtsstaat.

Es ist auch nicht so, dass besonders Menschen mit Migrationshintergrund eine Gefahr darstellen.
Wir hatten seit Solingen mehrere weitere Angriffe -- durch "Deutsche".
Teilweise weniger blutig, da schneller aufgehalten;
ich für meinen Teil bin darüber einfach froh zu sehen, dass "deutsche Effizienz" im Morden nicht (mehr) unsere Natur ist.

Aber selbst abseits dieser sicherheitspolitischen Überlegungen:
Der Vorschlag verstößt gegen Menschenrechte und gegen die Gleichheit vor dem Gesetz.
Man mag mich humanistisch, mglw. sogar christlich, geprägt nennen,
aber ich sehe keinen Grund warum zwei Menschen unterschiedlich behandelt werden,
bei gleicher Tat.
Erst recht nicht, wenn die Person die härter bestraft wird schon im Vorhinein die schlechteren
Ausgangsposition hat durch mögliches PTSD durch Krieg, Flucht und andere Traumata.
Wenn man, nur aufgrund dessen, dass man keinen deutschen Pass hat,
also einem Kriterium welches viel mit dem Glück des Geburtsortes zu tun hat,
nach Afghanistan abgeschoben wird und nicht in einem deutschen Gefängnis seine Rehabilitation antritt,
dann ist das nicht nur nicht rechtens, sondern einfach unmoralisch.
Mit Menschen so umzugehen, sie in Menschen erster Klasse ("dürfen" Gewalttaten verüben) und zweiter ("werden gegebenenfalls durch die Taliban umgebracht")
einzuteilen ist pervers.
Und nein, da gibt es kein "aber wir müssen doch":
Wir müssen damit leben, dass auch Menschen hier her kommen,
die ein komplett valides Schutzbedürfnis haben,
homosexuell sind, sich gegen das islamistische Regime gewendet haben,
einfach Frau sind, oder auch "einfach nur" vor Krieg flüchten -- und dann hier Gräueltaten verüben.
Auch deshalb, weil es nicht besser ist, wenn sie diese Taten in "ihrem Land" verüben würden;
außer man meint, dass Leben von Afghanen und Syrern weniger Wert sind.
Im Gegenteil, hier in Deutschland, mit einem funktionierenden Rechtsstaat,
würden diese Taten adäquat verfolgt werden,
höchstwahrscheinlich aufgeklärt und geahndet.

---

So viel zu Habeck.
Seit dem Statement gab es unterschiedlichste Forderungen von anderen Politikern.
Von Menschen die sich "sozial" und "demokratisch" nennen, zu "europäisch" und "freiheitlich" oder "christlich".
Und es grustelt mir,
wenn ich diese Menschen höre.
Sie fordern Maßnahmen, die nicht den Sozialstaat fördern,
sondern den Polizeistaat.
Die nationalistische Grenzkontrollen wollen.
Die antidemokratisch agieren und das Grundgesetz, sogar Menschenrechte "ohne Tabus"
angehen möchten.
Die unser aller Freiheit einschränken wollen,
und anstatt mit Nächstenliebe und freundlicher Stärke mit Kurzschlussreaktionen und Gewalt agieren möchten.

Seit dem, wurden Syrien und Afghanistan, als "Nicht das Land der Träume" [^2] bezeichnet.
Länder in denen Folterung und Verstümmelung an der Tagesordnung sind.
In denen Frauen nicht frei sein können, Homosexualität sowieso verboten ist.
In diese Länder wollen wir also abschieben -- aber eine Botschaft dort unterhalten,
das ist uns zu brenzlig.
Nicht so lange ist es her, dn Syrien und Afghanistan, als "Nicht das Land der Träume" [^2] bezeichnet.
Länder in denen Folterung und Verstümmelung an der Tagesordnung sind.
In denen Frauen nicht frei sein können, Homosexualität sowieso verboten ist.
In diese Länder wollen wir also abschieben -- aber eine Botschaft dort unterhalten,
das ist uns zu brenzlig.
Nicht so lange ist es her, da haben wir Menschen von dort evakuiert(!).
Und seit dem ist es nicht besser geworden.
Wie kann ein Mensch, der sich als Christdemokrat sieht, sich so äußern?
Mit so viel Verachtung für die Leben der Menschen,
die dorthin abgeschoben werden sollen?
Es ist auch klar, dass die Taliban und IS Menschen, die aus Afghanistan geflohen sind,
nicht mit offenen Armen empfangen werden.
Diese Abschiebungen kommen einen Todesurteil gleich -- nur das wir nicht selber hier unsere Hände
dreckig machen möchten.
Stattdessen zahlen wir den Henkern noch Geld und finanzieren sie[^3].

Es ist so, als hätten die Politiker den Plan des IS "The Extinction of the Grayzone"[^4]
gelesen und gedacht:
Hört sich gut an, setzen wir so um; aber was haltet ihr von der Idee euch Terroristen noch dafür zu bezahlen?

Auch andere haben diese Ziele schon lange analysiert[^5]:
Diese Anschläge zeichnen sich nicht durch eine besondere Auswahl ihrer Opfer aus.
Es wird nicht durch den Mord ein direktes Ziel verfolgt,
wie bspw. durch die Erpressungen der RAF.
Weiterhin liegt die Arbiträrität der Opfer nicht an einer egal-Haltung,
sondern hat doppelten Zweck:
Werden Menschen wahllos getötet,
sind wir alle potentiell gefährdet.
Wir können uns mit den Opfern identifizieren,
haben Angst.
Würden bspw. nur hochrangige Politiker das Ziel,
könnte sich die Bevölkerung davon lösen und in Sicherheit wägen.
Ebenfalls macht diese Arbitrität es den Sicherheitsbehörden nahezu unmöglich
zu agieren.
Es gibt keine Strukturen, keine Anweisungen, kaum Kommunikation;
selbst ein Präventionsstaat würde hier kaum Möglichkeiten haben.
Auch geht es nicht darum wirklich "viel Schaden" anzurichten.
Eigentlich reicht sogar schon der Versuch eines Terroranschlags,
solange darüber detailliert und blutig berichtet wird.

Denn diese mediale Aufmerksamkeit ist der Wirkungsvektor.
Es wird über Bande gespielt,
keine echte Gefahr aufgebaut (dafür ist die Anzahl der Toten viel zu gering),
sondern durch das Zielen auf "alle",
die Unmöglichkeit des Verhinderns und die Spektakularität,
soll ein derartiges Unsicherheitsgefühl aufgebaut werden,
dass wir die von den Terroristen verhasste Freiheit selbst einschränken.
Sie müssen keine Ziele formulieren, das tun wir schon ohne Zutun.

Und in diesem Licht müssen wir die Vorschläge und Forderungen der Politiker sehen:
Als Gehilfen des IS, nicht Widerstand.
Als Kämpfer Seite-an-Seite gegen die Freiheit, die Gleichheit und Menschlichkeit.
Ob Willentlich oder nicht.

Wenn wir wirklich etwas gegen den IS tun möchten, und zwar hierzulande,
müssen wir uns in Besonnenheit und radikaler Nächstenliebe üben
(echte Christen würden sagen: "die andere Wange hinhalten").
Denn die Forderung "Ausländer", die nur einen verschwindend geringen Anteil
der Gewalttaten ausmachen, deshalb abzuschieben entbehrt jeder moralischen, aber auch 
sicherheitspolitischen Grundlage.
Warum schieben wir nicht jene Bevölkerungsgruppe ab die tatsächlich für 85% aller Morde verantwortlich ist -- Männer?
Aus den gleichen Gründen, warum wir nicht andere Menschen aufgrund ihrer Herkunft, sexuellen Orientierung und und geschlichtlicher Identität,
ihrer Hautfarbe, ihrer Behinderungen, usw. abschieben.
Es gibt nur (maximal) einen rechtlichen Grund für abgelehnte Asylanträge:
Keine Grundlage für das Schutzbedürfnis.

Übrigens funktioniert der nazistische Terror anders:
Dieser kehrt genau die Prinzipien des IS um.
Gezielte Angriffe auf Politiker, denn "wir" sollen uns nicht fürchten -- manche mögen sogar den Politikern das "gönnen".
Nur Politiker sollen erpresst werden,
während die Bevölkerung das als Einzelfall der sie nicht weiter tangiert abtun können.
Für diesen gezielten Terrorismus sind Strukturen nötig die theoretisch polizeilich durchsucht werden könnten.
Das passiert natürlich nicht -- zynisch muss man sogar bemerken, dass die Strukturen ganz offiziell gefördert werden:
Die Nazis in der Polizei haben ganz "legal" Zugriff auf Datenbanken die sie auch bereitwillig nutzen.
Fällt das auf passiert in der Regel -- nix.
Auch in anderen Fällen werden Angriffe von Nazis, wenn sie mehr durch Zufall denn alles andere passieren,
kaum geahndet[^6].
Ein friedliches Blockieren der Straße durch Aktivist\*innen der Letzten Generation verbüßt man stattdessen mit einer fast doppelt solangen Haft.



---

Die Medien spielen auch hier natürlich eine Mittäterschaft.
Sie lassen geübte Lügner behaupten, wir hätten mit Abstand die meisten Flüchtlinge aufgenommen[^7].
Das wäre natürlich einfach zu überprüfen,
schließlich hat die Türkei in etwa das Dreifache aufgenommen,
der Lebanon hat -- auf die Anzahl der vorherigen Einwohner -- das Sechzehnfache aufgenommen wie wir.
Es ist anmaßend was Herr Merz lügt, aber umso peinlicher, dass die Medien daraus keine Schlagzeilen generieren.

Man hätte auch größer angelegt über die drei Muslima berichten können,
die eine mit einem Messer bewaffnete Deutsche überwältigt haben und Schlimmeres verhinderten[^8].
Kurz nach Solingen, hätte Siegen trenden müssen, es Debatten über Deutsche geben müssen.
Stattdessen war der höchste Politiker der sich ernsthaft zu der Thematik und dankbar gegenüber den Heldinnen geäußert hat...
der Bürgermeister.
Kein Notstand wurde ausgerufen (oder Feiertag), kein Brennpunkt -- Stille.

Und gleichzeitig gewinnen die Nazis an Prozenten, sind in manchen Bundesländern bei einem Drittel der Stimmen.
Zur Erinnerung -- viele scheinen das wirklich nicht zu wissen! -- die NSDAP hatte in den letzten Wahlen Nov '32 auch nur 33,1% der Stimmen.
In den nächsten vier Monaten kam es zur Machtergreifung, unter anderem durch Mithilfe durch die Vorgängerpartei der Union.
Und damals wie heute, ist die einzige Eigenschaft der "Brandmauer" der Union, dass sie brennt, abbrennt:
Während Merz noch 2021 diese Brandmauer beschwört[^9], will er heute nichts mehr davon gehört haben[^10].

Ähnlich handelt mittlerweile die Partei der Freiheit, die auch damals sich nicht durch besonders großen Widerstand bekleckert hat.
Moralische Leitplanken unserer Gesellschaft werden als "Denkverbote" denunziert und klar signalisiert:
Menschenrechte sind für uns auch nur Tinte auf Papier,
das kann man ändern[^11].
Dieser Schlag ins Gesicht der Freiheit sitzt tief,
soll aber wohl als Posten-Garantie für die sonst bedeutungslose Partei im Kabinett Höcke dienen.

Derweil dreht sich die Welt weiter, Grenzkontrollen innerhalb der EU werden wieder eingeführt
und die Messerkontrollen am Hamburger Bahnhof umgesetzt -- wie zu erwarten mit nur sinnlosen "Treffern".
Ich und mein Taschenmesser auf dem Weg zum Picknick wären auch betroffen.
Sicherlich, auch Attentäter können Messer benutzen -- aber wenn das wirklich der Terrorprävention dienen würde,
dann hätten wir bei 5 Messern in 3 Stunden ein wirkliches Terrorismusproblem.
Dafür ist die Anzahl der Attentate doch vergleichsweise gering.

Am Ende sind es heute die migrantischen evangelischen Theologen die die Christlichkeit beschwören und im Stern(!)
einen Beitrag schreiben der jedem dieser Politiker jeden Tag vorgelesen werden sollte[^12].

---

Ich dagegen muss jetzt in mehr Angst leben als vorher.
Diese Entwicklungen bieten nämlich nicht mehr Sicherheit,
dafür kann es sein, dass ich morgen aufwache und erfahre, dass Freund\*innen von mir um 2:00 morgens abgeholt wurden.
Vielleicht erfahre ich ja dann ein paar Tage später von ihrem Ende auf dem Schaffot in Afghanistan.

Und politisch geht es mit solchen Kursansagen nur weiter nach rechts.
Die AfD wird weiter erstarken -- die Hoffnung, dass die Wähler nun jetzt umschwenken ist absurd: Die wählen weiter "das Original".
Und damit muss auch ich mir Sorge um mein Leben und meine Freiheit machen.
Die AfD würde mich am Liebsten gleich mit deportieren.
Wenigstens gehe ich dann den letzten Weg meiner Freund\*innen mit ihnen.



[^1]: https://www.welt.de/politik/video253175916/So-aeussert-sich-Habeck-zum-Messeranschlag-von-Solingen-Waffenrecht-muss-verschaerft-werden.html
[^2]: https://www.focus.de/politik/deutschland/nach-anschlag-in-solingen-merz-fordert-aufnahmestopp-fuer-afghanen-und-syrer-nicht-die-messer-sind-das-problem_id_260253533.html
[^3]: https://www.spiegel.de/politik/deutschland/abschiebungen-spd-experte-haelt-verhandlungen-mit-taliban-und-assad-fuer-unausweichlich-a-84fbb178-a884-4561-8dac-62e1c21899a3
[^4]: https://www.washingtonpost.com/news/worldviews/wp/2015/11/16/the-islamic-state-wants-you-to-hate-refugees/
[^5]: https://books.google.de/books?id=swJkERVd80cC&newbks=1&newbks_redir=0&printsec=frontcover&pg=PA78&dq=%22Very+often+the+goal+of+a+particular+action%22&hl=en&redir_esc=y#v=onepage&q=%22Very%20often%20the%20goal%20of%20a%20particular%20action%22&f=false
[^6]: https://www.ndr.de/nachrichten/niedersachsen/Milde-Strafen-fuer-Angriff-auf-Goettinger-Journalisten,fretterode106.html
[^7]: https://www.youtube.com/live/J6jejbGnEME?feature=shared&t=2925
[^8]: https://www.tagesschau.de/siegen-nordrhein-westfalen-bus-messerattacke-100.html
[^9]: https://www.spiegel.de/politik/deutschland/friedrich-merz-geht-auf-angela-merkel-zu-a-86dd9f24-51b1-4df8-b509-923bfca2e2fb
[^10]: https://www.n-tv.de/politik/Merz-Behauptung-stimmt-nicht-und-wirft-Fragen-auf-article25205275.html
[^11]: https://x.com/c_lindner/status/1832080125111791906
[^12]: https://www.stern.de/kultur/solingen-und-friedrich-merz--schluss-mit-der-angst-vorm-messermann-35017914.html
