---
title: Cracking "dash-dash-x"
author: lj
date: 2023-08-17
keywords: [linux, ctf, reversing, assembly, c, ldpreload, debugging]
---

The Chaos Communication Congress '23 hosted a CTF with a neat challenge:
Extract a flag that, while completely in plain text,
was stored in a binary you could only execute but not read.
Thus having the permissions `--x--x--x`.

Additionally,
[source code in C](https://ljrk.codeberg.page/blog/cccamp23-ctf-dash-dash-x/flagcheck.c)
to the challenge was provided,
but of course not with the actual flag but a test value.

# First Ideas

Since the executable file isn't readable,
the first idea was to read the process memory instead.
The approach was to run and immediately suspend the process,
then read its process memory from `/proc/$PID/mem`.
Unfortunately for us, this approach was foiled by `mem` not being readable either :(

Since the system allowed `ptrace` for child processes,
we also copied a static `gdb` binary to the system.
However, `gdb` wouldn't run a binary that it couldn't read.
Various other tools such as `xocopy` didn't work for us.

The idea that worked out in the end was to use good ol' `LD_PRELOAD` to execute code in the context of the process.

# Hooking `strlen`

The code uses the libc function `strlen` which is dynamically loaded from glibc.
Using `LD_PRELOAD` we can override this `strlen` implementation and
substitute it with our own.

```c
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <dlfcn.h>

static size_t (*real_strlen)(const char*);
static void hook(void)
{
	real_strlen = dlsym(RTLD_NEXT, "strlen");
	if (!real_strlen) {
		fprintf(stderr, "dlsym: %s\n", dlerror());
		exit(1);
	}
}

size_t strlen(const char *const s)
{
	if (!real_strlen) { hook(); }
	
	const size_t ret = real_strlen(s);
	fprintf(stderr, "strlen(\"%s\") = %zu\n", s, ret);

	return (ret);
}
```

We can compile and link
[this code](https://ljrk.codeberg.page/blog/cccamp23-ctf-dash-dash-x/pwn1.c)
into a shared object/dynamically loaded library:
```
$ c99 -fpic -shared -o pwn1.so pwn1.c
```

Afterwards, preload it while running our program:
```
$ LD_PRELOAD=$PWD/pwn1.so flagcheck A
strlen("A") = 1
strlen("A") = 1
wrong :(
```

As we can see, `strlen` is only called on the user input --
and not on the flag itself.
This is to be expected,
since the flag is string literal and most compilers optimize `strlen` calls to these and calculate the length at compile time.

No cookie?
Don't despair, we still get useful stuff out of this!

# Detour: Being correctly incorrect

Curiously, the call to `strlen` happens twice!
This is due to the expansion of the `MIN` macro in `test_flag`:

```c
bool test_flag(char *inp)
{
	bool res = true;
	int n = MIN(strlen(FLAG), strlen(inp));
	for (int i = 0; i <= n; i++) {
		res &= FLAG[i] == inp[i];
	}
	return res;
}
```

The macro `MIN(A, B)` expands to [^1]:
```c
A < B ? A : B
```
With `B` being `strlen(inp)` this will indeed generate two calls to the function with the same input!
We can use this to violate a base assumption of this code:
That both calls will return the same value!
This is partly due to the first return value being treated "fully" as a `size_t` in the comparison,
the second being truncated to `int` in the assignment.

One goal could be to make `n` negative in order to skip the loop
(the value 0 wouldn't suffice, then we could just run the program with an empty string).
This can only be done through our `B` being negative.
However, since the comparison is between two unsigned values,
this can never be true.

Except, if the two calls return different values!
In this case the code would more aptly be described as:
```c
A < B1 ? A : B2
```
Then, we can make the first call return something small for `B1` (e.g., 0) s.t.,
the comparison `A < B1` will be false and `B2` will be assigned to `n`.
Second, the `B2` could be `(size_t)-1` which is a very high `size_t` value,
but will be assigned as `-1` to `n`.

Adjusting our `strlen` function
[like so](https://ljrk.codeberg.page/blog/cccamp23-ctf-dash-dash-x/pwn2.c):
```c
size_t strlen(const char *const s)
{
	static int first_call = 1;

	if (!real_strlen) { hook(); }

	int dead = -1;
	size_t ret = first_call ? 0 : (size_t)dead;
	fprintf(stderr, "strlen(\"%s\") = %zu (%d)\n", s, ret, (int)ret);

	first_call = 0;
	return (ret);
}
```

This indeed makes our program behave differently :-)
```
$ LD_PRELOAD=$PWD/pwn2.so flagcheck aaaa
strlen("aaaa") = 0 (0)
strlen("aaaa") = 18446744073709551615 (-1)
correct!
```

A different approach is to always make `strlen` return 0,
while passing a string that's of length 1,
since we know the first character of the flag: `A`.
That way, the loop does run but only iterate once comparing our
input with the first character of the flag, which should then match.

Unfortunately, neither approach helps us much in getting the flag :'D

# Dumping memory

The glibc has a useful builtin function to dump us the address that
will be returned to after the currently running function finishes.
Since `strlen` is called rather early in `test_hook`,
we simply subtract 32 from this address and write the following 256 bytes to a file,
effectively
[dumping the code of this function](https://ljrk.codeberg.page/blog/cccamp23-ctf-dash-dash-x/pwn2.c):
```c
size_t strlen(const char *const s)
{
	(void)s;

	static int first_call = 1;

	if (!real_strlen) { hook(); }

	if (first_call) {
		const int fd = open("test_hook.bin", O_RDWR | O_CREAT, 0666);
		if (fd < 0) {
			fprintf(stderr, "open: test_hook.bin: %s\n", strerror(errno));
			exit(1);
		}

		const char *const test_hook = __builtin_return_address(0) - 0x20;
		fprintf(stderr, "test_hook: approx %p\n", (const void*)test_hook);
		assert(write(fd, test_hook, 256) == 256);
		close(fd);
	}

	first_call = 0;

	return (0);
}
```

Running this code yields a file containing the machine code of at this position in memory:
```
$ LD_PRELOAD=$PWD/pwn3.so flagcheck aaaa
test_hook: approx 0x563fe397f169
wrong :(
$ ls -l test_hook.bin 
-rw-r--r-- 1 ctf ctf 256 Aug 17 20:00 test_hook.bin 
```

We can then instruct `objdump` to disassemble this "raw" x86\_64 machine code (not in ELF format)
in intel syntax:

```
$ objdump -M intel -m i386:x86-64 -b binary -D test_hook.bin 

test_hook.bin:     file format binary


Disassembly of section .data:

0000000000000000 <.data>:
   0:	f3 0f 1e fa          	endbr64 
   4:	55                   	push   rbp
   5:	48 89 e5             	mov    rbp,rsp
   8:	48 83 ec 20          	sub    rsp,0x20
   c:	48 89 7d e8          	mov    QWORD PTR [rbp-0x18],rdi
  10:	c6 45 f7 01          	mov    BYTE PTR [rbp-0x9],0x1
  14:	48 8b 45 e8          	mov    rax,QWORD PTR [rbp-0x18]
  18:	48 89 c7             	mov    rdi,rax
  1b:	e8 e7 fe ff ff       	call   0xffffffffffffff07
  20:	48 83 f8 48          	cmp    rax,0x48
  24:	77 0e                	ja     0x34
  26:	48 8b 45 e8          	mov    rax,QWORD PTR [rbp-0x18]
  2a:	48 89 c7             	mov    rdi,rax
  2d:	e8 d5 fe ff ff       	call   0xffffffffffffff07
  32:	eb 05                	jmp    0x39
  34:	b8 48 00 00 00       	mov    eax,0x48
  39:	89 45 fc             	mov    DWORD PTR [rbp-0x4],eax
  3c:	c7 45 f8 00 00 00 00 	mov    DWORD PTR [rbp-0x8],0x0
  43:	eb 3a                	jmp    0x7f
  45:	0f b6 4d f7          	movzx  ecx,BYTE PTR [rbp-0x9]
  49:	8b 45 f8             	mov    eax,DWORD PTR [rbp-0x8]
  4c:	48 98                	cdqe   
  4e:	48 8d 15 4a 0e 00 00 	lea    rdx,[rip+0xe4a]        # 0xe9f
  55:	0f b6 14 10          	movzx  edx,BYTE PTR [rax+rdx*1]
  59:	8b 45 f8             	mov    eax,DWORD PTR [rbp-0x8]
  5c:	48 63 f0             	movsxd rsi,eax
  5f:	48 8b 45 e8          	mov    rax,QWORD PTR [rbp-0x18]
  63:	48 01 f0             	add    rax,rsi
  66:	0f b6 00             	movzx  eax,BYTE PTR [rax]
  69:	38 c2                	cmp    dl,al
  6b:	0f 94 c0             	sete   al
  6e:	0f b6 c0             	movzx  eax,al
  71:	21 c8                	and    eax,ecx
  73:	85 c0                	test   eax,eax
  75:	0f 95 c0             	setne  al
  78:	88 45 f7             	mov    BYTE PTR [rbp-0x9],al
  7b:	83 45 f8 01          	add    DWORD PTR [rbp-0x8],0x1
  7f:	8b 45 f8             	mov    eax,DWORD PTR [rbp-0x8]
  82:	3b 45 fc             	cmp    eax,DWORD PTR [rbp-0x4]
  85:	7e be                	jle    0x45
  87:	0f b6 45 f7          	movzx  eax,BYTE PTR [rbp-0x9]
  8b:	c9                   	leave  
  8c:	c3                   	ret    
  8d:	f3 0f 1e fa          	endbr64 
  91:	55                   	push   rbp
  92:	48 89 e5             	mov    rbp,rsp
  95:	48 83 ec 10          	sub    rsp,0x10
  99:	89 7d fc             	mov    DWORD PTR [rbp-0x4],edi
  9c:	48 89 75 f0          	mov    QWORD PTR [rbp-0x10],rsi
  a0:	83 7d fc 01          	cmp    DWORD PTR [rbp-0x4],0x1
  a4:	7f 16                	jg     0xbc
  a6:	48 8d 05 3b 0e 00 00 	lea    rax,[rip+0xe3b]        # 0xee8
  ad:	48 89 c7             	mov    rdi,rax
  b0:	e8 42 fe ff ff       	call   0xfffffffffffffef7
  b5:	b8 01 00 00 00       	mov    eax,0x1
  ba:	eb 3c                	jmp    0xf8
  bc:	48 8b 45 f0          	mov    rax,QWORD PTR [rbp-0x10]
  c0:	48 83 c0 08          	add    rax,0x8
  c4:	48 8b 00             	mov    rax,QWORD PTR [rax]
  c7:	48 89 c7             	mov    rdi,rax
  ca:	e8 31 ff ff ff       	call   0x0
  cf:	84 c0                	test   al,al
  d1:	74 11                	je     0xe4
  d3:	48 8d 05 24 0e 00 00 	lea    rax,[rip+0xe24]        # 0xefe
  da:	48 89 c7             	mov    rdi,rax
  dd:	e8 15 fe ff ff       	call   0xfffffffffffffef7
  e2:	eb 0f                	jmp    0xf3
  e4:	48 8d 05 1c 0e 00 00 	lea    rax,[rip+0xe1c]        # 0xf07
  eb:	48 89 c7             	mov    rdi,rax
  ee:	e8 04 fe ff ff       	call   0xfffffffffffffef7
  f3:	b8 00 00 00 00       	mov    eax,0x0
  f8:	c9                   	leave  
  f9:	c3                   	ret    
  fa:	00 f3                	add    bl,dh
  fc:	0f 1e fa             	nop    edx
  ff:	48                   	rex.W
```

Our guess of 32 bytes was pretty accurate, we were wrong by 4 bytes,
as the fifth byte (second line) in the output is the functions prelude.

The call at 1b is the call to our strlen,
in 20 the result is compared to 64 (probably the length of the actual flag).
If not, `strlen` is called again, with the function's result expected in `rax` (26-32).
The next calls set `n` to this value (39),
initialize `i` to 0 and jump to the for loop condition (43) at 7f.
Then, we jump to the function body (85) at 45 where `inp[i]` is loaded and,
more interestingly,
our `FLAG[i]` (a6).

We can see that our flag is loaded relatively to the current instruction,
with an offset of +0xe3b,
so approximately 3500 bytes after the start of the function.
Simply
[increasing the 256 bytes to 4096](https://ljrk.codeberg.page/blog/cccamp23-ctf-dash-dash-x/pwn2.c)
will dump us enough of the memory to contain our string:
```
$ strings test_hook.bin 
ALLES!{surprisingly_similar_to_setuid_processes_but_not_actually_secure}
usage: flagcheck FLAG
correct!
wrong :(
:*3$"
```

And indeed, we've solved the challenge :-)
```
$ flagcheck 'ALLES!{surprisingly_similar_to_setuid_processes_but_not_actually_secure}'
correct!
```



[^1]: Actually to `(((A) < (B)) ? (A) : (B))`
