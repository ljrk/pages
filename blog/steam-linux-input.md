---
title: Joystick Input on Steam and Linux
date: 2021-09-06
author:
- ljrk
keywords: [linux, joysticks, steam, proton, wine, xinput, dinput, evdev, joydev]
---

Joysticks are complicated and making them run on any system can be quite a
PITA.
Also, the ecosystem is constantly evolving and there's a decidedly big amount
of documentation that's only readable if you are in the in-group of people
already understanding what all the abbreviations etc. stand for.

While the post is titled "and Linux" I will first start by talking about
Windows and then Wine, to come back to native Linux later.

# Windows Input APIs

Windows has two major APIs for applications using joysticks, dinput and xinput.
Next to the joystick-specific API it also supports generic Human Interface
Devices (HID) with an API appropriately called hid as well.

Since a joystick is a HID device (or can function as one), you can access
joysticks on Windows often through all three APIs, although I wouldn't recommend
going the HID approach.

The dinput API is the old DirectX Input API and has rather limited functionality.
In fact, this API is simply a joystick-specific wrapper around the HID driver.
Both HID and Dinput don't support features like rumble (they are *input*, not
*output* devices).
Additionally, Dinput forced a deadzone onto the joystick, if wanted or not.

The more modern Xinput API is implemented by the joystick driver directly and
doesn't go the route via HID.

# Wine Dinput/Xinput implementations

While Wine does have a Dinput and a HID implementation, the former wasn't
originally based on the latter.
That is, since each operating system has it's own API or (APIs) to talk
to joysticks (Windows: dinput/xinput as seen, Linux: joydev/evdev), the
reimplementation of Dinput in Wine is operating system specific.
There's one Dinput implementation that handles joydev/evdev input, and one
that works on whatever input system OSX/macOS uses.

    device -> linux_dinput   \
    device -> osx_dinput     | (dinput)
    device -> freebsd_dinput /


With Wine 6.16 this is being rewritten, as the HID layer in Wine already
does this system-specific abstraction:

    device -> linux_HID   \
    device -> osx_HID     | (HID) -> hid_dinput
    device -> freebsd_HID /

Now, for Xinput, in Wine this is implemented by leveraging SDL2.
So we have

    device -> SDL2 -> xinput

Where SDL2 is a big blob that handles our Linux/OSX/FreeBSD specifica.

# Linux Input APIs and Wine/SDL2 woes

As already mentioned, Linux has two APIs as well.
The joystick-specific joydev API as well as a more modern generic lowlevel
API called evdev.

Just like Dinput, joydev forced a deadzone, while evdev doesn't---it only
recommends a deadzone setting which the application can honor or not.

SDL2, when switching from the joydev API to the evdev API for its own
implementation simply applied the evdev-recommended deadzone to stay consistent
with the old behavior.

Now remember that Wine uses SDL2 to implement xinput?
And Xinput doesn't set a deadzone?

This was indeed a problem but with SDL2 2.0.14 we can ask SDL2 to not apply
the deadzone and Wine's Xinput implementation works as intended.

Meanwhile, you could use a udev rule to set the deadzone value that evdev would
eventually recommend to SDL2 to zero.

# Steam Input

Steam Input is an abstraction over *any* input device.
It leverages SDL2 as a backend to provide additional features ontop.

There's a good overview of the concepts
[here](https://partner.steamgames.com/doc/features/steam_controller).

Basically we have two different modes, Legacy and Native.

## Legacy

In Legacy mode, Steam Input is just a way to remap arbitrary buttons.
You can use any controller supported by SDL2 and remap its "triangle button"
to "X" or whatever.
This doesn't require any support by the game, to the game the device is just
a generic Xbox/Xinput controller (since that's what most games support).

## Native

This requires cooperation by the game developer.
They now instead of doing:

    if (joystick.button_x.is_pressed()) { shoot(); }

or:

    if (config.shoot_button.is_pressed()) { shoot(); }

they use/define "actions".
In a classic shooter this would mean that the developers provide an extra file
that lists all actions user can make (pseudocode again!):

    # In-Menu
    Previous
    Next
    Select

    # In-Game
    Shoot
    Move Left
    Move Right
    Move Up
    Move Down
    Jump

In their code they now write:

    if (steam_input.shoot.is_pressed()) { shoot(); }

Configuring which button refers to which action is done using the
Steam Input Configurator.
As we can see in the mapping above, we don't even have only actions but
action sets.
And for each set, the user can configure a different mapping.

## Steam Input on Wine: Open Questions

Since Steam Input uses SDL2 it works for Linux and Windows games in
precisely the same way.
But what if we run a Windows game using Proton/Wine on Linux?

If the game itself uses the Xinput API, implemented in Wine using SDL2, where
does the Steam Input "Legacy" Remapper come into place?
Does it run as a Windows library like this

    device/evdev -> SDL2 -> Wine -> SteamRemapWin32 -> Game

or does it run before Wine:

    device/evdev -> [SDL2 -> SteamRemapLinux]-> [SDL2 -> Wine] -> Game

And, if the game uses the Steam Input "Native" API with action sets, does
our stack now look like this?

    device/evdev -> SDL2 -> Wine -> SteamInputWin32 -> Game

or like this, circumventing Wine for the input layer altogether?

    device/evdev -> SDL2 -> SteamInputLinux -> Game

## Further Notes

Many games use SDL2 themselves to abstract over Xinput/Evdev/..., so many
more modern games don't even use XInput anymore, even on Windows, so the
second graphic should look like?

    device/evdev -> [SDL2 -> SteamRemapLinux]-> [SDL2 -> Wine] -> [SDL2 -> Game]

