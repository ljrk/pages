---
title: Addressing "Concerned Citizens"
date: 2024-10-17
author:
- ljrk
keywords: [politics, extremism, trolling, activism]
---

This is a "troll" speech that came to me while on the bike.
It's not really polished or good, but also was just too much fun to write.

# To "Concerned Citizens"

In this speech I want to talk about you.
You, the ones the media calls "concerned citizens".
I want to talk about what it means to be a concerned citizen, and what it doesn't.
What it means for the movement.

Indeed, when the media calls you that, they're dishonest.
They don't want to call you, what you are: Citizens, with a goal.
Everyone of us, we aren't "concerned citizens",
we are citizens with an idea how this country should change,
how it should be formed.
Instead of this, you are called "concerned".

But you aren't people who want someone, say, a social democrat,
to come and hold your hand while listening to your concerns,
while trying to persuade you otherwise.
No, none of us are social democrats or want the social democrats.

However, the term "concerned citizen" is not completely without merit.
It has it's use indeed against the very mass media that labels you so.
Because concerns must be heard —
and the only ones who disagree with that are those on the far left,
which just serves to ostracize them even more.
Saying "concerned citizen" opens a door where otherwise it might be closed.
Because stating your goals would not fall on fertile ears.
Concern, however, worry, that is something almost everybody understands.

Moreover, a true concerned citizen would falter and
stray from their path when faced with arguments addressing that concern
— something that cannot happen to a citizen who's only concerned about their goal.
They will stride on, proud,
striking arguments that should calm their worries aside.
Facts, they don't concern you!
